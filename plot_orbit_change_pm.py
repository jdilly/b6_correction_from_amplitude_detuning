""" Quick plot of different orbits for the 2022 b6 paper."""
from plotting import plot_ip  # in another package, sorry
from matplotlib import pyplot as plt
from pathlib import Path
import os

model_plus = "/home/jdilly/Software/b6_correction_from_amplitude_detuning/md6863_with_ip5_inj_tunes/ip5+_b1/twiss.lhc.b1.nominal.tfs"
model_minus = "/home/jdilly/Software/b6_correction_from_amplitude_detuning/md6863_with_ip5_inj_tunes/ip5-_b1/twiss.lhc.b1.nominal.tfs"

if __name__ == '__main__':
    os.chdir("/home/jdilly/Software/b6_correction_from_amplitude_detuning/md6863_with_ip5_inj_tunes/")
    plot_ip.main(
        components=("magnets", "orbit"),
        ip="IP5",
        data={"+160": model_plus, "-160": model_minus},
        ylim_orbit=[-8.2, 8.2],
        manual_style={u"figure.figsize": [6.5, 3.5],
                  u'savefig.format': u'pdf'},
        out_name="orbit_160pm"
    )
    
    # plt.show()