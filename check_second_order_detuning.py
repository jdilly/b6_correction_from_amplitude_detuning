"""
Check the influence of second order detuning
"""
from pathlib import Path

import pandas as pd

import tfs
from utilities.lhc_simulation import get_detuning_from_ptc_output


def get_detuning_terms_ptc(folder: Path, beam: int, id_: str):
    columns = ["X10", "Y01", "X01", "Y10", "X20", "X11", "X02", "Y20", "Y11", "Y02"]
    df = tfs.read(folder / f"b{beam}" / f"ampdet.lhc.b{beam}.{id_}.tfs")
    df_nominal = tfs.read(folder / f"b{beam}" / f"ampdet.lhc.b{beam}.nominal.tfs")
    detuning = pd.Series(get_detuning_from_ptc_output(df, log=False, terms=columns))
    detuning_nominal = pd.Series(get_detuning_from_ptc_output(df_nominal, log=False, terms=columns))
    return detuning - detuning_nominal


def get_detuning_terms(folder: Path, beam: int, id_: str):
    df = tfs.read(folder / f"b{beam}" / f"ampdet_calc.lhc.b{beam}.{id_}.tfs")
    columns = [c for c in df.columns if c[0] in "XY"]
    return df.query("FIELDS == 'b6' and IP == 'all'").iloc[0][columns]


def calculate_detuning(terms: pd.Series, action: float):
    print(f"Detuning at {action*1e6:.3f}um action:")
    for term in terms.index:
        dx = int(term[1])
        dy = int(term[2])
        order = dx + dy
        detuning = terms[term] * (action**order)
        if dx == 2 or dy == 2:
            detuning = detuning * 0.5
        print(f"{term}: {detuning:.1e}")


def compare_terms(terms_calc, terms_ptc):
    print(f"     Calculated       PTC      Diff      %")
    for term in terms_ptc.index:
        calc = terms_calc[term]
        ptc = terms_ptc[term]
        print(f"{term}: {calc: 10.1e} {ptc: 10.1e} {calc-ptc: 10.1e} {(calc-ptc)/ptc*100: 4.1f}%")


if __name__ == '__main__':
    folder = Path("md6863_with_ip5_inj_tunes")
    terms_calc = get_detuning_terms(folder, beam=1, id_="X10X01Y01_full_and_ip5pm_b6")
    terms_ptc = get_detuning_terms_ptc(folder, beam=1, id_="X10X01Y01_full_and_ip5pm_b6_inj_tunes")
    compare_terms(terms_calc, terms_ptc)
    # calculate_detuning(terms, action=0.01e-6)