import itertools
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import tfs

from matplotlib import colors as mc
from correction_via_amplitude_detuning import fakeLHCBeam
from utilities.detuning_calculation import DODECAPOLE_CORRECTOR, get_detuning_coeff
from utilities.lhc_simulation import LHCBeam
from omc3.plotting.utils import colors as pcolors, style as pstyle, annotations as pannot

DETUNING_TERMS = ("X10", "X01", "Y01")
INSTANCES = 100_000


def create_itertable_df(misalignment, bbeat) -> tfs.TfsDataFrame:
    params = ("BETX", "BETY", "X", "Y")
    columns = [f"{side}{ip}{param}" for ip in (1, 5) for side in "LR" for param in params]

    df = tfs.TfsDataFrame(0.0, index=range(INSTANCES), columns=columns)
    for column in df.columns:
        df.loc[:, column] = np.random.normal(loc=0.0, scale=bbeat if "BET" in column else misalignment, size=INSTANCES)
        # test for uniform misalignments:
        # if "BET" not in column:
        #     df.loc[:, column] = np.random.uniform(low=-misalignment, high=misalignment, size=INSTANCES)
    return df



def main(input_dir, correction_name, misalignment, bbeat, output_dir):
    inputdirs = {b: input_dir / f"b{b}" for b in (1, 4)}

    lhc_beams_in = {b: fakeLHCBeam(beam=b, outputdir=indir) for b, indir in inputdirs.items()}
    optics = {lhc_in.beam: tfs.read(LHCBeam.output_path(lhc_in, 'twiss', correction_name), index="NAME") for lhc_in in lhc_beams_in.values()}
    q = {b: tfs.TfsDataFrame(0.,
                             columns=DETUNING_TERMS,
                             index=["value", "min", "max", "mean", "std", "absmin"],
                             headers={"Beam": b, "BetaBeat": bbeat, "Misalignment": misalignment})
         for b in inputdirs.keys()}

    for beam, twiss in optics.items():
        for term in DETUNING_TERMS:
            for ip in (1, 5):
                for side in "LR":
                    magnet = DODECAPOLE_CORRECTOR.format(side=side, ip=ip)
                    beta = {p: twiss.loc[magnet, f"BET{p}"] for p in "XY"}
                    x, y = twiss.loc[magnet, "X"], twiss.loc[magnet, "Y"]
                    # # this is a check for the influence of the non-zero orbit:
                    # x, y = 0, 0
                    coeff = get_detuning_coeff(term, beta)
                    k6l = twiss.loc[magnet, "K5L"]
                    q[beam].loc["value", term] += 0.5 * (x**2 - y**2) * coeff * k6l

    iterable_df = create_itertable_df(misalignment=misalignment, bbeat=bbeat)
    q_iter = {b: tfs.TfsDataFrame(0., columns=DETUNING_TERMS, index=iterable_df.index, headers={"Beam": b}) for b in inputdirs.keys()}
    for beam, twiss in optics.items():
        for term in DETUNING_TERMS:
            for ip in (1, 5):
                for side in "LR":
                    name = f"{side}{ip}"
                    magnet = DODECAPOLE_CORRECTOR.format(side=side, ip=ip)
                    beta = {p: twiss.loc[magnet, f"BET{p}"] * (1+iterable_df[f"{name}BET{p}"]) for p in "XY"}
                    x, y = twiss.loc[magnet, "X"]+iterable_df[f"{name}X"], twiss.loc[magnet, "Y"]+iterable_df[f"{name}Y"]
                    # this is a check for the influence of the non-zero orbit:
                    # x, y = iterable_df[f"{name}X"], iterable_df[f"{name}Y"]
                    coeff = get_detuning_coeff(term, beta)
                    k6l = twiss.loc[magnet, "K5L"]
                    q_iter[beam].loc[:, term] += 0.5 * (x ** 2 - y ** 2) * coeff * k6l

        if output_dir:
            tfs.write(output_dir / f"seeds_{get_name(misalignment, bbeat, beam)}.tfs", q_iter[beam],
                      save_index="Realization")

    for beam in q.keys():
        q[beam].loc["min", :] = q_iter[beam].min()
        q[beam].loc["absmin", :] = q_iter[beam].abs().min()
        q[beam].loc["max", :] = q_iter[beam].max()
        q[beam].loc["mean", :] = q_iter[beam].mean()
        q[beam].loc["std", :] = q_iter[beam].std()
        if output_dir:
            tfs.write(output_dir / f"stats_{get_name(misalignment, bbeat, beam)}.tfs", q[beam],
                      save_index="Statistic")
    return q

def get_name(misalignment, bbeat, beam):
    return f"{misalignment * 1e3:0.2f}mm_{bbeat:0.2f}beat_b{beam}"


def plot_histogramm(output_dir, misalignment, bbeat):
    manual_style = {
        # "font.size": 22.5,
        # "figure.constrained_layout.use": False,
        "figure.figsize": [4.2, 3.4],
        "figure.subplot.left": 0.12,
        "figure.subplot.bottom": 0.15,
        "figure.subplot.right": 0.99,
        "figure.subplot.top": 0.77,
        "errorbar.capsize": 5,
        "axes.grid": False,
        "xtick.minor.visible": True,
    }
    pstyle.set_style(manual=manual_style)
    name_map = {"X10": "$Q_{x,x}$", "X01": "$Q_{x,y}$", "Y01": "$Q_{y,y}$"}

    xlim, ylim = None, None
    if misalignment == 1e-3 and bbeat == 0.05:
        xlim = [-70, 70]
        # ylim = [0, 180]


    for beam in (1, 4):
        fig, ax = plt.subplots()
        name = get_name(misalignment, bbeat, beam)
        df_seeds = tfs.read(output_dir / f"seeds_{name}.tfs",
                            index="Realization")
        df_stats = tfs.read(output_dir / f"stats_{name}.tfs",
                            index="Statistic")
        for idx, (term, label) in enumerate(name_map.items()):
            color = pcolors.get_mpl_color(idx)
            ax.hist(
                (df_seeds[term] - df_stats.loc["value", term]) * 1e-3,
                histtype='stepfilled',
                bins=int(INSTANCES / 100),
                density=True,
                color=(*mc.to_rgb(color), 0.05),
                edgecolor=color,
                label=label
            )
            for sign in (1, -1):
                ax.axvline(sign*df_stats.loc["std", term] * 1e-3, ls="--", marker="None", color=color, zorder=-1)
            # ax.hist(
            #     df_seeds[term] - df_stats.loc["value", term],
            #     bins=int(INSTANCES / 50),
            #     # kde=True,
            #     alpha=0.1,
            #     color=color,
            #     edgecolor="None",
            #     )
        ax.set_xlabel(r"Change in Detuning $\left[10^{3} \mathrm{m}^{-1}\right]$")
        ax.set_ylabel("Density")
        if xlim:
            ax.set_xlim(xlim)
        if ylim:
            ax.set_ylim(ylim)

        ax.set_yticks([])
        pannot.make_top_legend(ax, ncol=3)
        fig.savefig(output_dir / f"plot.{name}.pdf")
        # plt.show()
        # return


def create_all(input_dir, id_, output_dir):
    for misalignment in (0.0, 0.5e-3, 1e-3, 2e-3):
        for bbeat in (0.0, 0.05, 0.1):
            if misalignment == 0.0 and bbeat == 0.0:
                continue

            res = main(input_dir, id_, misalignment=misalignment, bbeat=bbeat, output_dir=output_dir)


def plot_all(output_dir):
    for misalignment in (0.0, 0.5e-3, 1e-3, 2e-3):
        for bbeat in (0.0, 0.05, 0.1):
            if misalignment == 0.0 and bbeat == 0.0:
                continue

            plot_histogramm(output_dir, misalignment=misalignment, bbeat=bbeat)




def compare_this_and_misalignment_study(out_dir: Path):
    this = tfs.read("/home/jdilly/Software/b6_correction_from_amplitude_detuning/md6863_with_ip5/b1/twiss.lhc.b1.X10X01Y01_full_and_ip5pm_b6.tfs", index="NAME")
    mslgn = tfs.read("/home/jdilly/Work/study.20.irnl_misalignment_study/testrun/Outputdata/b1.misaligned.1.corrector_twiss.tfs", index="NAME")
    df = pd.DataFrame()
    for ip in (1, 5):
        for side in "LR":
            magnet = DODECAPOLE_CORRECTOR.format(side=side, ip=ip)
            for twiss, label in ((this, "md6863"),  (mslgn, "2018")):
                for column in ("BETX", "BETY", "X", "Y", "K5L"):
                    df.loc[magnet, f"{column}_{label}"] = twiss.loc[magnet, column]
    tfs.write_tfs(out_dir  / "compare.tfs",  df)
    # for term in DETUNING_TERMS:
    #     for ip in (1, 5):
    #         for side in "LR":
    #             name = f"{side}{ip}"
    #             magnet = DODECAPOLE_CORRECTOR.format(side=side, ip=ip)
    #             beta = {p: twiss.loc[magnet, f"BET{p}"] * (1 + iterable_df[f"{name}BET{p}"]) for p in "XY"}
    #             x, y = twiss.loc[magnet, "X"] + iterable_df[f"{name}X"], twiss.loc[magnet, "Y"] + iterable_df[f"{name}Y"]
    #             coeff = get_first_order_coeff(term, beta)
    #             k6l = twiss.loc[magnet, "K5L"]
    #             q_iter[beam].loc[:, term] += 0.5 * (x ** 2 - y ** 2) * coeff * k6l

if __name__ == '__main__':
    # out_dir = Path("misalign_and_betabeat_uniform_zeroorbit")
    # out_dir.mkdir(exist_ok=True)
    # res = main(Path("md6863_with_ip5_inj_tunes"),
    #            "X10X01Y01_full_and_ip5pm_b6_inj_tunes",
    #            misalignment=1e-3,
    #            bbeat=0.0,
    #            output_dir=out_dir)
    # plot_histogramm(out_dir, misalignment=1e-3, bbeat=0.0)
    out_dir = Path("misalign_and_betabeat")
    out_dir.mkdir(exist_ok=True)
    create_all(Path("md6863_with_ip5_inj_tunes"), "X10X01Y01_full_and_ip5pm_b6_inj_tunes", out_dir)
    plot_all(out_dir)
    # out_dir = Path("misalign_and_betabeat_col_tunes")
    # out_dir.mkdir(exist_ok=True)
    # create_all(Path("md6863_with_ip5_col_tunes"), "X10X01Y01_full_and_ip5pm_b6_col_tunes", out_dir)
    # plot_all(out_dir)
    # compare_this_and_misalignment_study(out_dir)


    # create_itertable()