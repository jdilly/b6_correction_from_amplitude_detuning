"""
MD6863 - Detuning as measured during MD6863 (24.06.2022)
--------------------------------------------------------

This data is used to calculated b6 corrections.
Note that accidentally the wrong tunes (collision instead of injection)
were used in the simulation!!

"""
import shutil
from pathlib import Path
from typing import Dict, Any, Sequence

import tfs

from correction_via_amplitude_detuning import main as run_correction
from correction_via_amplitude_detuning_multi_xing import calculate_corrections
from utilities.detuning_calculation import DODECAPOLE_CORRECTOR
from utilities.utils import log_setup
from utilities.classes import scaled_detuning, scaled_contraints, scaled_detuningmeasurement, TargetData, Target, \
    DetuningMeasurement, Detuning
from utilities.plotting import plot_detuning_ips, plot_correctors, get_corrector_strengths


def get_sum(meas_a, meas_b):
    return {beam: meas_a[beam] + meas_b[beam] for beam in meas_a.keys()}


def get_diff(meas_a, meas_b):
    return {beam: meas_a[beam] - meas_b[beam] for beam in meas_a.keys()}

# Define Machine Data
# -------------------

# Output Path
output = Path("md6863_tmptest")

# Fill in special values for crossing if needed:
xing = {'scheme': 'flat', 'on_x1': -160, 'on_x5': 160}

# Fill in measurement data in 10^3 m^-1:
# meas_flat = {
#     1: scaled_detuningmeasurement(X10=(-15.4, 0.9), X01=(33.7, 1), Y01=(-8.4, 0.5)),
#     2: scaled_detuningmeasurement(X10=(-8.7, 0.7), X01=(13, 2), Y01=(10, 0.9)),
# }
# meas_full = {
#     1: scaled_detuningmeasurement(X10=(20, 4), X01=(43, 4), Y01=(-10, 3)),
#     2: scaled_detuningmeasurement(X10=(26, 0.8), X01=(-27, 4), Y01=(18, 7)),
# }
# Fill in measurement data in 10^3 m^-1:
meas_flat = {
    1: scaled_detuningmeasurement(X10=(-18, 2), X01=(23.5, 11.5), Y01=(0.2, 1)),  # X01: 15,3  32, 3
    2: scaled_detuningmeasurement(X10=(-22, 2), X01=(13.5, 3.5), Y01=(3.6, 0.9)),  # X01: 13, 3  14, 3
}
meas_full = {
    1: scaled_detuningmeasurement(X10=(10, 2), X01=(31.5, 7.5), Y01=(1.4, 0.9)),  # X01: 37, 2  26, 2
    2: scaled_detuningmeasurement(X10=(22, 1), X01=(-45, 5), Y01=(20, 2)),  # X01: -47, 3  -43, 3
}
# meas_ip5 = {
#     1: scaled_detuningmeasurement(X10=(0, 0), X01=(0, 0), Y01=(0, 0)),
#     2: scaled_detuningmeasurement(X10=(0, 0), X01=(0, 0), Y01=(0, 0)),
# }
# meas_ip1 = {
#     1: scaled_detuningmeasurement(X10=(0, 0), X01=(0, 0), Y01=(0, 0)),
#     2: scaled_detuningmeasurement(X10=(0, 0), X01=(0, 0), Y01=(0, 0)),
# }

# If one or the other was not measured (for local corrections in IP)

# meas_ip1 = get_sum(get_diff(meas_full, meas_ip5), meas_flat)
# meas_ip5 = get_sum(get_diff(meas_full, meas_ip1), meas_flat)


# Steps of calculations --------------------------------------------------------

def ltx_dqd2j(tune, action, power=1):
    if power == 1:
        return f"$Q_{{{tune},{action}}}$"
    return f"$Q_{{{tune},{action}^{{{power}}}}}$"


def get_detuning(meas: Dict[Any, DetuningMeasurement]) -> Dict[Any, Detuning]:
    return {beam: meas[beam].get_detuning() for beam in meas.keys()}


def get_targets() -> Sequence[Target]:
    # detuning_flat = get_detuning(meas_flat)
    # detuning_full = get_detuning(meas_full)
    detuning_flat = meas_flat
    detuning_full = meas_full
    targets = [
        Target(
            name="X10X01Y01_global",
            data=[
                TargetData(
                    ips=(1, 5),
                    detuning=get_diff(detuning_flat, detuning_full),
                    xing='',  # label for output path
                ),
            ]
        ),
        # # TARGET TEMPLATE
        # Target(
        #     name="X10X01Y01_",
        #     data=[
        #         TargetData(
        #             ips=(1, ),
        #             detuning={
        #                 1: scaled_detuning(X10=0, X01=0, Y01=0),
        #                 2: scaled_detuning(X10=0, X01=0, Y01=0),
        #             },
        #             constraints={
        #                 1: scaled_constraints(X01="<=0"),
        #                 2: scaled_constraints(X01="<=0"),
        #             }
        #         ),
        #     ]
        # )
    ]
    # print(targets)
    # exit()
    return targets


def simulation():
    paths = {i: output / f"b{i}" for i in (1, 4)}
    run_correction(paths, xing=xing, targets=get_targets(), year=2022)


def do_correction_only():
    paths = {i: output / f"b{i}" for i in (1, 4)}
    calculate_corrections(paths, targets=get_targets(), main_xing='')


def plotting():
    action_map = {"10": 'x', "01": 'y'}

    nchar = 10
    measurement = get_diff(meas_flat, meas_full)
    measurement[4] = measurement.pop(2)

    targets = get_targets()
    ids = [f"{target.name}_b6" for target in targets]
    terms = ("X10", "X01", "Y01")
    scaled = [{term: targets[0].data[0][b][term]*1e-3 for term in terms} for b in (1, 2)]
    labels = [
        "\n".join([
            f"{ltx_dqd2j(term[0], action_map[term[1:]])} = {f'{scaled[0][term]:.1f} | {scaled[1][term]}'.center(nchar)}"
            for term in terms
            ]
        )
    ]

    output_id = f"_corrections"
    plot_detuning_ips(
        output,
        ids=ids,
        labels=labels,
        fields="b6",
        size=[6., 3.9],
        measurement=measurement,
        beams=(1, 4),
        ylims={1: [-62, 62], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id,
        alternative="separate",  # "separate", "normal"
        delta=True,
    )
    corr_size = [4.80, 4.00]

    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=f'{output_id}_b6')
    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}_b6_ip5')
    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}_b6_ip1')


def plot_rounding_error_comp():
    """ Compare the before and after rounding error data ..."""
    targets = get_targets()
    ids = [f"{target.name}_b6" for target in targets]

    rounded_folder = Path("rounding_error") / output
    for beam in (1, 4):
        beam_dir = output / f"b{beam}"
        beam_dir_rounded = rounded_folder / f"b{beam}"
        for id_ in ids:
            id_rounded = f"{id_}_rounded"
            for filename in ("settings.lhc.b{beam}.{id_}.madx", "ampdet_calc.lhc.b{beam}.{id_}.tfs"):
                file_in = beam_dir_rounded / filename.format(beam=beam, id_=id_)
                file_out = beam_dir / filename.format(beam=beam, id_=id_rounded)
                shutil.copy(file_in, file_out)
    ids = ids + [f"{id_}_rounded" for id_ in ids]

    labels = ["not rounded", "rounded"]

    output_id = f"_corrections_comparisons"
    corr_size = [4.80, 4.00]

    # plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=output_id)

    for beam in (1, 4):
        data = {id_: get_corrector_strengths(output, beam, id_, corrector_mask='kctx3\.[lr]\d') for id_ in ids}
        print(f"Beam {beam}")
        for corrector in data[ids[0]].keys():
            print(f"{corrector}: {(data[ids[1]][corrector] / data[ids[0]][corrector]) - 1}")


    for beam in (1, 4):
        data = {id_: tfs.read_tfs(output / f"b{beam}" / f"ampdet_calc.lhc.b{beam}.{id_}.tfs") for id_ in ids}
        print(f"Beam {beam}")
        data_rounded = data[ids[1]].query("FIELDS == 'b6' & IP == 'all'")[["X10", "X01", "Y01"]].iloc[0]
        data = data[ids[0]].query("FIELDS == 'b6' & IP == 'all'")[["X10", "X01", "Y01"]].iloc[0]
        diff_data = data_rounded.abs() - data.abs()
        beat_data = diff_data / data.abs() * 100
        for term in data_rounded.index:
            print(f"{term}: {round(diff_data[term])} ,   {round(beat_data[term])}%")
        pass




if __name__ == '__main__':
    log_setup()
    # simulation()
    do_correction_only()
    # plotting()
    # plot_rounding_error_comp()
