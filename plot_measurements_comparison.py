"""
Plotter for differences in Measurements.

Data to plot:
  Comissioning:
  - (Xing - Flat) before b6
  - Expected after correction: (Xing + Correction) - Flat
  - (Xing - Flat) after b6


  MD6863:
  - (Xing - Flat) before b6
  - Expected after correction: (Xing + Correction) - Flat
  - (Xing - Flat) after b6


  Increase during years
  - Flat 2022 - Flat 2021



"""
from pathlib import Path

from utilities.classes import scaled_detuningmeasurement, MeasureValue, DetuningMeasurement
from utilities.plotting import get_all_detuning_data
from utilities.utils import log_setup
from utilities.plot_utils_measurements_comparison import plot_measurements, MeasurementSetup, get_ylabel

from setup_MD6863 import output as folder, get_targets
from setup_MD6863_with_ip5 import output as folder_improved, get_targets as get_targets_improved
from setup_commish_2022 import output as folder_commish, get_targets as get_targets_commish
from omc3.plotting.utils import colors as pcolors

import logging

LOG = logging.getLogger(__name__)

SAVE_FIGS = True


data = dict(
    commish2022_nob6_flat={
        1: scaled_detuningmeasurement(X10=(-15.4, 0.9), X01=(32.2, 2), Y10=(33.7, 1.0), Y01=(-8.4, 0.5)),
        2: scaled_detuningmeasurement(X10=(-8.7, 0.7), X01=(13, 2), Y10=(-3, 2), Y01=(10, 0.9)),
    },
    commish2022_nob6_xing={
        1: scaled_detuningmeasurement(X10=(20, 4), X01=(43, 4), Y10=(33, 10), Y01=(-10, 3)),
        2: scaled_detuningmeasurement(X10=(26, 0.8), X01=(-31, 3), Y10=(-27, 4), Y01=(18, 7)),
    },
    commish2022_wb6_flat={
        1: scaled_detuningmeasurement(X10=(-34, 7), X01=(38, 3), Y10=(24, 4), Y01=(-6, 1)),
        2: scaled_detuningmeasurement(X10=(-15, 2), X01=(10, 3), Y10=(6.3, 0.4), Y01=(-3.6, 0.6)),
    },
    commish2022_wb6_xing={
         1: scaled_detuningmeasurement(X10=(-21, 4), X01=(47, 2), Y10=(56, 6), Y01=(15, 1)),
         2: scaled_detuningmeasurement(X10=(-28, 2), X01=(22, 4), Y10=(13, 2), Y01=(-7.8, 0.5)),
    },
    md6863_nob6_flat={
         1: scaled_detuningmeasurement(X10=(-18, 2), X01=(32, 3), Y10=(15, 3), Y01=(0.2, 1)),
         2: scaled_detuningmeasurement(X10=(-22, 2), X01=(14, 3), Y10=(13, 3), Y01=(3.6, 0.9)),
    },
    md6863_nob6_xing={
         1: scaled_detuningmeasurement(X10=(10, 2), X01=(37, 2), Y10=(26, 2), Y01=(1.4, 0.9)),
         2: scaled_detuningmeasurement(X10=(22, 1), X01=(-47, 3), Y10=(-43, 3), Y01=(20, 2)),
    },
    md6863_nob6_IP5p_xing={
         1: scaled_detuningmeasurement(X10=(23, 2), X01=(2, 3), Y10=(-4, 1), Y01=(3, 1)),
         2: scaled_detuningmeasurement(X10=(11, 1), X01=(-8, 3), Y10=(-17, 2), Y01=(5, 1)),
    },
    md6863_nob6_IP5m_xing={
         1: scaled_detuningmeasurement(X10=(9, 1), X01=(3, 4), Y10=(-0.9, 0.6), Y01=(0.3, 0.5)),
         2: scaled_detuningmeasurement(X10=(20, 2), X01=(-15, 3), Y10=(-24, 2), Y01=(-2, 2)),
    },
    md6863_wb6_xing={
         1: scaled_detuningmeasurement(X10=(-12.5, 0.9), X01=(34, 2), Y10=(29.9, 1.0), Y01=(17, 1)),
         2: scaled_detuningmeasurement(X10=(-45, 4), X01=(31, 2), Y10=(35, 1), Y01=(-18, 1)),
    },
    # ---vvv--- From print_correction_expectation from below ---vvv---
    commish2022_corrections={
        1: scaled_detuningmeasurement(X10=(-20,), X01=(5,), Y01=(15,)),
        2: scaled_detuningmeasurement(X10=(-38,), X01=(39,), Y01=(-10,)),
    },
    md6863_corrections={
        1: scaled_detuningmeasurement(X10=(-16,), X01=(5,), Y01=(10,)),
        2: scaled_detuningmeasurement(X10=(-48,), X01=(56,), Y01=(-21,)),
    },
    # md6863_with_ip5={  # collision tunes (wrong)
    #     1: scaled_detuningmeasurement(X10=(-19,), X01=(6,), Y01=(12,)),
    #     2: scaled_detuningmeasurement(X10=(-44,), X01=(54,), Y01=(-23,)),
    # },
    md6863_with_ip5={  # injection tunes (correct)
        1: scaled_detuningmeasurement(X10=(-20,), X01=(7,), Y01=(12,)),
        2: scaled_detuningmeasurement(X10=(-50,), X01=(53,), Y01=(-23,)),
    },
    # ={
    #     1: scaled_detuningmeasurement(X10=(,), X01=(,), Y10=(,), Y01=(,)),
    #     2: scaled_detuningmeasurement(X10=(,), X01=(,), Y10=(,), Y01=(,)),
    # },

)


def average_crossterm(d):
    d_new = {}
    for measurement in d.keys():
        d_new[measurement] = {}
        for beam, meas in d[measurement].items():
            if meas.X01 and meas.Y10:
                av_cross = (meas.X01 + meas.Y10) / 2
            else:
                av_cross = meas.X01 or meas.Y10
            d_new[measurement][beam] = DetuningMeasurement(X10=meas.X10, X01=av_cross, Y01=meas.Y01)
    return d_new


data = average_crossterm(data)

PLOT_TERMS = ["X10", "X01", "Y01"]


def add_limit_detuning(ax):
    return   # skip
    for y in (-12, 12):
        ax.axhline(y, ls="--", c="k", zorder=-5, marker="", alpha=0.3, lw=0.5)



def print_correction_expectation():
    """ Get the difference PTC-nominal from the simulated values."""
    def print_out(label):
        scale = 1e-3
        return f"{label}=({round(measurement['X01' if label == 'Y10' else label]*scale)},)"

    folder_id_map = {
        "md6863": "X10X01Y01_global_b6",
        # "commish2022": "X10X01Y01_global_b6",
        "md6863_with_ip5_old_wrong_tunes": "X10X01Y01_full_and_ip5pm_b6",
        "md6863_with_ip5_inj_tunes": "X10X01Y01_full_and_ip5pm_b6_inj_tunes",
        "md6863_with_ip5_col_tunes": "X10X01Y01_full_and_ip5pm_b6_col_tunes",
    }

    for folder, id_ in folder_id_map.items():
        data, data_calc = get_all_detuning_data(folder=Path(folder), ids=(id_,))
        print(folder)
        for beam, all_ids in data.items():
            beam = 2 if beam == 4 else beam
            measurement = all_ids[id_]
            # data_str = ", ".join([print_out(l) for l in Detuning.fieldnames()])
            data_str = ", ".join([print_out(l) for l in PLOT_TERMS])
            print(f"{beam}: scaled_detuningmeasurement({data_str}),")
        print()


def plot_commissioning():
    """
    To Plot:
      - (Xing - Flat) before b6
      - Expected after correction: (Xing + Correction) - Flat
      - (Xing - Flat) after b6
    """
    out_dir = Path("commish2022")
    for beam in (1, 2):
        nob6 = data["commish2022_nob6_xing"][beam] - data["commish2022_nob6_flat"][beam]
        est = nob6 + data["commish2022_corrections"][beam]
        wb6 = data["commish2022_wb6_xing"][beam] - data["commish2022_wb6_flat"][beam]
        meas = [
            MeasurementSetup(measurement=nob6, label="w/o b6 corr."),
            MeasurementSetup(measurement=est, label="estimated"),
            MeasurementSetup(measurement=wb6, label="w/ b6 corr."),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-70, 70],
                                add_rms=True,
                                )
        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_change_corrections.b{beam:d}.pdf")
        
        text = ""
        for term in nob6.terms():
            text += (
                f"{term}:\n"
                f"w/o b6: {str(nob6[term])}\n"
                f"estimated: {str(est[term])}\n"
                f"w/ b6: {str(wb6[term])}\n"
            )
        text += (
            f"RMS:\n"
            f"w/o b6: {str(MeasureValue.weighted_rms([nob6[t] for t in nob6.terms()]))}\n"
            f"estimated: {str(MeasureValue.weighted_rms([est[t] for t in est.terms()]))}\n"
            f"w/ b6: {str(MeasureValue.weighted_rms([wb6[t] for t in wb6.terms()]))}\n"
        )
        LOG.info("\n" + text)
        (out_dir / f"data.detuning_change_corrections.b{beam:d}.txt").write_text(text)


def plot_commissioning_and_corrections():
    """
    To Plot:
      - (Xing - Flat) before b6
      - Expected after correction: (Xing + Correction) - Flat
      - (Xing - Flat) after b6
    """
    out_dir = Path("commish2022")
    for beam in (1, 2):
        nob6 = data["commish2022_nob6_xing"][beam] - data["commish2022_nob6_flat"][beam]
        est = nob6 + data["commish2022_corrections"][beam]
        wb6 = data["commish2022_wb6_xing"][beam] - data["commish2022_wb6_flat"][beam]
        meas = [
            MeasurementSetup(measurement=nob6, label="w/o b6 corr."),
            MeasurementSetup(measurement=est, label="estimated"),
            MeasurementSetup(measurement=wb6, label="w/ b6 corr."),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-70, 70],
                                add_rms=True,
                                )
        ax = fig.gca()

        targets = get_targets_commish()  # is only 1
        ids = [f"{target.name}_b6" for target in targets]
        bar_width = 0.33
        stack_width = 0.15 * bar_width
        measurement_width = 1 / (len(meas) + 1)
        color_bar = pcolors.get_mpl_color(0)
        scale = 1e-3

        load_beam = beam
        if beam == 2:
            load_beam = 4
        corr_data, _ = get_all_detuning_data(folder_commish, ids, (load_beam,), for_ips=True)

        for idx_term, term in enumerate(PLOT_TERMS):
            for idx_id, id_name in enumerate(ids):

                x_pos = idx_term + 1 * measurement_width
                term = "X01" if term == "Y10" else term
                y_pos = corr_data[load_beam][id_name][term]*scale
                ax.bar(x_pos, -y_pos, stack_width, bottom=0, label=f"_{beam}.{id_name}",
                       color=color_bar, alpha=0.3)

        add_limit_detuning(ax)

        # plt.show()
        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_and_corr.b{beam:d}.pdf")


def plot_md6863():
    """
    To Plot:
      - (Xing - Flat) before b6
      - Expected after correction: (Xing + Correction) - Flat
      - (Xing - Flat) after b6
    """
    out_dir = Path("md6863")
    for beam in (1, 2):
        nob6 = data["md6863_nob6_xing"][beam] - data["md6863_nob6_flat"][beam]
        est = nob6 + data["md6863_corrections"][beam]
        wb6 = data["md6863_wb6_xing"][beam] - data["md6863_nob6_flat"][beam]
        meas = [
            MeasurementSetup(measurement=nob6, label="w/o b6 corr."),
            MeasurementSetup(measurement=est, label="estimated"),
            MeasurementSetup(measurement=wb6, label="w/ b6 corr."),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-70, 70],
                                add_rms=True,
                                )

        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_change_corrections.b{beam:d}.pdf")

        text = ""
        for term in nob6.terms():
            text += (
                f"{term}:\n"
                f"w/o b6: {str(nob6[term])}\n"
                f"estimated: {str(est[term])}\n"
                f"w/ b6: {str(wb6[term])}\n"
            )
        text += (
            f"RMS:\n"
            f"w/o b6: {str(MeasureValue.weighted_rms([nob6[t] for t in nob6.terms()]))}\n"
            f"estimated: {str(MeasureValue.weighted_rms([est[t] for t in est.terms()]))}\n"
            f"w/ b6: {str(MeasureValue.weighted_rms([wb6[t] for t in wb6.terms()]))}\n"
        )
        LOG.info("\n" + text)
        (out_dir / f"data.detuning_change_corrections.b{beam:d}.txt").write_text(text)


def plot_md6863_and_corrections():
    """
    To Plot:
      - (Xing - Flat) before b6
      - Expected after correction: (Xing + Correction) - Flat
      - (Xing - Flat) after b6
    """
    out_dir = Path("md6863")
    for beam in (1, 2):
        nob6 = data["md6863_nob6_xing"][beam] - data["md6863_nob6_flat"][beam]
        est = nob6 + data["md6863_corrections"][beam]
        wb6 = data["md6863_wb6_xing"][beam] - data["md6863_nob6_flat"][beam]
        meas = [
            MeasurementSetup(measurement=nob6, label="w/o b6 corr."),
            MeasurementSetup(measurement=est, label="estimated"),
            MeasurementSetup(measurement=wb6, label="w/ b6 corr."),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-70, 70],
                                add_rms=True,
                                )
        ax = fig.gca()

        targets = get_targets()  # is only 1
        ids = [f"{target.name}_b6" for target in targets]
        bar_width = 0.33
        stack_width = 0.15 * bar_width
        measurement_width = 1 / (len(meas) + 1)
        color_bar = pcolors.get_mpl_color(0)
        scale = 1e-3

        load_beam = beam
        if beam == 2:
            load_beam = 4
        corr_data, _ = get_all_detuning_data(folder, ids, (load_beam,), for_ips=True)

        for idx_term, term in enumerate(PLOT_TERMS):
            for idx_id, id_name in enumerate(ids):

                x_pos = idx_term + 1 * measurement_width
                term = "X01" if term == "Y10" else term
                y_pos = corr_data[load_beam][id_name][term]*scale
                ax.bar(x_pos, -y_pos, stack_width, bottom=0, label=f"_{beam}.{id_name}",
                       color=color_bar, alpha=0.3)

        add_limit_detuning(ax)
        # plt.show()
        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_and_corr.b{beam:d}.pdf")


def plot_md6863_ip5_b5b6():
    """
    To Plot:
      - IP5+ - Flat
      - b6 contribution: (IP5+ + IP5- - 2Flat) / 2
      - b5 contribution: (IP5+ - IP5-)/2
    """
    out_dir = Path("md6863")
    for beam in (1, 2):
        full = (data["md6863_nob6_IP5p_xing"][beam] - data["md6863_nob6_flat"][beam])
        from_b6 = (data["md6863_nob6_IP5p_xing"][beam] + data["md6863_nob6_IP5m_xing"][beam]) * 0.5 - data["md6863_nob6_flat"][beam]
        from_b5 = (data["md6863_nob6_IP5p_xing"][beam] - data["md6863_nob6_IP5m_xing"][beam]) * 0.5
        meas = [
            MeasurementSetup(measurement=full, label="IP5 +160$\mu rad$"),
            MeasurementSetup(measurement=from_b5, label="from $b_5$"),
            MeasurementSetup(measurement=from_b6, label="from $b_6$"),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-70, 70],
                                # add_rms=True,
                                )
        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_ip5_b5b6.b{beam:d}.pdf")

        text = ""
        for term in full.terms():
            text += (
                f"{term}:\n"
                f"All: {str(full[term])}\n"
                f"From b5: {str(from_b5[term])} ({from_b5.get_detuning()[term] / full.get_detuning()[term] * 100:.1f}%)\n"
                f"From b6: {str(from_b6[term])} ({from_b6.get_detuning()[term] / full.get_detuning()[term] * 100:.1f}%)\n"
            )
        LOG.info("\n" + text)
        (out_dir / f"data.detuning_ip5_b5b6.b{beam:d}.txt").write_text(text)


def plot_md6863_ip5_b5b6_and_corrections():
    """
    To Plot:
      - IP5+ - Flat
      - b6 contribution: (IP5+ + IP5- - 2Flat) / 2
      - b5 contribution: (IP5+ - IP5-)/2
    """
    out_dir = Path("md6863")
    for beam in (1, 2):
        full = (data["md6863_nob6_IP5p_xing"][beam] - data["md6863_nob6_flat"][beam])
        from_b6 = (data["md6863_nob6_IP5p_xing"][beam] + data["md6863_nob6_IP5m_xing"][beam]) * 0.5 - data["md6863_nob6_flat"][beam]
        from_b5 = (data["md6863_nob6_IP5p_xing"][beam] - data["md6863_nob6_IP5m_xing"][beam]) * 0.5
        meas = [
            MeasurementSetup(measurement=full, label="IP5 +160$\mu rad$"),
            MeasurementSetup(measurement=from_b5, label="from $b_5$"),
            MeasurementSetup(measurement=from_b6, label="from $b_6$"),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-70, 70],
                                add_rms=True,
                                )
        ax = fig.gca()

        targets = get_targets()  # is only 1
        ids = [f"{target.name}_b6" for target in targets]
        bar_width = 0.33
        stack_width = 0.15 * bar_width
        measurement_width = 1 / (len(meas) + 1)
        color_bar = pcolors.get_mpl_color(2)
        scale = 1e-3

        load_beam = beam
        if beam == 2:
            load_beam = 4
        _, calculated_data = get_all_detuning_data(folder, ids, (load_beam,), for_ips=True)

        for idx_term, term in enumerate(PLOT_TERMS):
            for idx_id, id_name in enumerate(ids):
                # Plot calculated Data:

                x_pos = idx_term + 3 * measurement_width
                term = "X01" if term == "Y10" else term
                y_pos = calculated_data[load_beam][id_name].loc["5", term]*scale
                ax.bar(x_pos, -y_pos, stack_width, bottom=0, label=f"_{beam}.{id_name}",
                       color=color_bar, alpha=0.3)

        # plt.show()
        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_ip5_b5b6_and_corr.b{beam:d}.pdf")


def plot_md6863_ip5_ip1():
    """
    To Plot:
      - dF = Full - Flat
      - d5 = IP5+ - Flat
      - d1 = Full - IP5+
    From:
    d5 = 5 - 0
    dF = F - 0 = d1 + d5
    d1 = 1 - 0
    d1 = F - 0 - d5 = F - 0 - 5 + 0 = F - 5
    """
    out_dir = Path("md6863")
    for beam in (1, 2):
        full = (data["md6863_nob6_xing"][beam] - data["md6863_nob6_flat"][beam])
        ip5 = (data["md6863_nob6_IP5p_xing"][beam] - data["md6863_nob6_flat"][beam])
        ip1 = (data["md6863_nob6_xing"][beam] - data["md6863_nob6_IP5p_xing"][beam])
        meas = [
            MeasurementSetup(measurement=full, label="Full Xing"),
            MeasurementSetup(measurement=ip5, label="from IP5"),
            MeasurementSetup(measurement=ip1, label="from IP1"),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-70, 70],
                                add_rms=True,
                                )
        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_ip5_ip1.b{beam:d}.pdf")

        text = ""
        for term in full.terms():
            text += (
                f"{term}:\n"
                f"Full Xing: {str(full[term])}\n"
                f"From IP5: {str(ip5[term])} ({ip5.get_detuning()[term] / full.get_detuning()[term] * 100:.1f}%)\n"
                f"From IP1: {str(ip1[term])} ({ip1.get_detuning()[term] / full.get_detuning()[term] * 100:.1f}%)\n"
            )
        LOG.info("\n" + text)
        (out_dir / f"data.detuning_ip5_ip1.b{beam:d}.txt").write_text(text)


def plot_md6863_ip5_ip1_and_corrections():
    """
    To Plot:
      - dF = Full - Flat
      - d5 = IP5+ - Flat
      - d1 = Full - IP5+
    From:
    d5 = 5 - 0
    dF = F - 0 = d1 + d5
    d1 = 1 - 0
    d1 = F - 0 - d5 = F - 0 - 5 + 0 = F - 5
    """
    out_dir = Path("md6863")
    for beam in (1, 2):
        full = (data["md6863_nob6_xing"][beam] - data["md6863_nob6_flat"][beam])
        ip5 = (data["md6863_nob6_IP5p_xing"][beam] - data["md6863_nob6_flat"][beam])
        ip1 = (data["md6863_nob6_xing"][beam] - data["md6863_nob6_IP5p_xing"][beam])
        meas = [
            MeasurementSetup(measurement=full, label="Full Xing"),
            MeasurementSetup(measurement=ip5, label="from IP5"),
            MeasurementSetup(measurement=ip1, label="from IP1"),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-70, 70],
                                add_rms=True,
                                )
        ax = fig.gca()

        targets = get_targets()  # is only 1
        ids = [f"{target.name}_b6" for target in targets]
        bar_width = 0.33
        stack_width = 0.15 * bar_width
        measurement_width = 1 / (len(meas) + 1)
        color_all = pcolors.get_mpl_color(0)
        color_ip5 = pcolors.get_mpl_color(1)
        color_ip1 = pcolors.get_mpl_color(2)
        scale = 1e-3

        load_beam = beam
        if beam == 2:
            load_beam = 4
        _, calculated_data = get_all_detuning_data(folder, ids, (load_beam,), for_ips=True)

        for idx_term, term in enumerate(PLOT_TERMS):
            for idx_id, id_name in enumerate(ids):

                # Plot calculated Data:
                calc = {f: calculated_data[load_beam][id_name].loc[f, term]*scale for f in ("5", "1", "all")}

                x_pos = idx_term + 1 * measurement_width
                ax.bar(x_pos, -calc["all"], stack_width, bottom=0, label=f"_{beam}.{id_name}.all", color=color_all, alpha=0.3)

                x_pos = idx_term + 2 * measurement_width
                ax.bar(x_pos, -calc["5"], stack_width, bottom=0, label=f"_{beam}.{id_name}.ip5", color=color_ip5, alpha=0.3)

                x_pos = idx_term + 3 * measurement_width
                ax.bar(x_pos, -calc["1"], stack_width, bottom=0, label=f"_{beam}.{id_name}.ip1", color=color_ip1, alpha=0.3)

        # plt.show()
        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_ip5_ip1_and_corr.b{beam:d}.pdf")


def plot_md6863_ip5_ip1_b6_with_new_corrections():
    """
    To Plot:
      - dF = Full - Flat
      - d5 = IP5+ - Flat
      - d1 = Full - IP5+
      - b6 contribution: (IP5+ + IP5- - 2Flat) / 2
    From:
    d5 = 5 - 0
    dF = F - 0 = d1 + d5
    d1 = 1 - 0
    d1 = F - 0 - d5 = F - 0 - 5 + 0 = F - 5
    """
    out_dir = Path("md6863_with_ip5_inj_tunes")
    for beam in (1, 2):
        full = (data["md6863_nob6_xing"][beam] - data["md6863_nob6_flat"][beam])
        ip5 = (data["md6863_nob6_IP5p_xing"][beam] - data["md6863_nob6_flat"][beam])
        ip1 = (data["md6863_nob6_xing"][beam] - data["md6863_nob6_IP5p_xing"][beam])
        from_ip5_b6 = (data["md6863_nob6_IP5p_xing"][beam] + data["md6863_nob6_IP5m_xing"][beam]) * 0.5 - data["md6863_nob6_flat"][beam]
        est = full + data["md6863_with_ip5"][beam]
        meas = [
            MeasurementSetup(measurement=full, label="Full Xing"),
            MeasurementSetup(measurement=ip5, label="from IP5"),
            MeasurementSetup(measurement=from_ip5_b6, label="from IP5 $b_6$"),
            MeasurementSetup(measurement=ip1, label="from IP1"),
            MeasurementSetup(measurement=est, label="estimated"),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-70, 70],
                                add_rms=True,
                                )
        ax = fig.gca()

        targets = get_targets()  # is only 1
        ids = [f"{target.name}_b6" for target in targets]
        bar_width = 0.33
        stack_width = 0.15 * bar_width
        measurement_width = 1 / (len(meas) + 1)
        color_all = pcolors.get_mpl_color(0)
        color_ip5 = pcolors.get_mpl_color(1)
        color_ip5_b6 = pcolors.get_mpl_color(2)
        color_ip1 = pcolors.get_mpl_color(3)
        scale = 1e-3

        load_beam = beam
        if beam == 2:
            load_beam = 4
        _, calculated_data = get_all_detuning_data(folder_improved, ids, (load_beam,), for_ips=True)

        for idx_term, term in enumerate(PLOT_TERMS):
            for idx_id, id_name in enumerate(ids):

                # Plot calculated Data:
                calc = {f: calculated_data[load_beam][id_name].loc[f, term]*scale for f in ("5", "1", "all")}

                x_pos = idx_term + 1 * measurement_width
                ax.bar(x_pos, -calc["all"], stack_width, bottom=0, label=f"_{beam}.{id_name}.all", color=color_all, alpha=0.3)

                x_pos = idx_term + 2 * measurement_width
                ax.bar(x_pos, -calc["5"], stack_width, bottom=0, label=f"_{beam}.{id_name}.ip5", color=color_ip5, alpha=0.3)

                x_pos = idx_term + 3 * measurement_width
                ax.bar(x_pos, -calc["5"], stack_width, bottom=0, label=f"_{beam}.{id_name}.ip5_b6", color=color_ip5_b6, alpha=0.3)

                x_pos = idx_term + 4 * measurement_width
                ax.bar(x_pos, -calc["1"], stack_width, bottom=0, label=f"_{beam}.{id_name}.ip1", color=color_ip1, alpha=0.3)

        add_limit_detuning(ax)
        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_ip5_ip5b6_ip1_and_corr.b{beam:d}.pdf")

        text = ""
        for term in full.terms():
            text += (
                f"{term}:\n"
                f"full: {str(full[term])}\n"
                f"ip5: {str(ip5[term])}\n"
                f"ip1: {str(ip1[term])}\n"
                f"ip5_b6: {str(from_ip5_b6[term])}\n"
                f"est: {str(est[term])}\n"
            )
        text += (
            f"RMS:\n"
            f"full: {str(MeasureValue.weighted_rms([full[t] for t in full.terms()]))}\n"
            f"ip5: {str(MeasureValue.weighted_rms([ip5[t] for t in ip5.terms()]))}\n"
            f"ip1: {str(MeasureValue.weighted_rms([ip1[t] for t in ip1.terms()]))}\n"
            f"ip5_b6: {str(MeasureValue.weighted_rms([from_ip5_b6[t] for t in from_ip5_b6.terms()]))}\n"
            f"est: {str(MeasureValue.weighted_rms([est[t] for t in est.terms()]))}\n"
        )
        # LOG.info("\n" + text)
        (out_dir / f"data.detuning_change_corrections.b{beam:d}.txt").write_text(text)


if __name__ == '__main__':
    log_setup()
    # print_correction_expectation()

    manual_style = {
        # "font.size": 22.5,
        # "figure.constrained_layout.use": False,
        "figure.figsize": [6.50, 3.0],
        "figure.subplot.left": 0.12,
        "figure.subplot.bottom": 0.15,
        "figure.subplot.right": 0.99,
        "figure.subplot.top": 0.77,
        "errorbar.capsize": 5,
        "lines.marker": "x",
        "lines.markersize": 4,
        "axes.grid": False,
        "ytick.minor.visible": True,
    }
    # plot_commissioning()
    # plot_md6863()
    # plot_md6863_ip5_b5b6()
    # plot_md6863_ip5_ip1()
    # plot_commissioning_and_corrections()
    # plot_md6863_and_corrections()
    # plot_md6863_ip5_ip1_and_corrections()
    # plot_md6863_ip5_b5b6_and_corrections()
    plot_md6863_ip5_ip1_b6_with_new_corrections()
    # print_correction_expectation()
    # plt.show()
