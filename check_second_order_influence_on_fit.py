"""
Calculate the influence of the second order detuning on the fit.
This is done by using the second order detuning as calculated from
the expected correction and subtracting it from the measured data.
A new fit on this shifted data is then calculated and the difference to
the original measurement calculated.
This gives an estimate for the error on the measurement.
"""
import re
from pathlib import Path
from typing import Tuple, Sequence, Union

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import tfs
from generic_parser import DotDict
from omc3.amplitude_detuning_analysis import single_action_analysis, get_kick_and_bbq_df
from omc3.tune_analysis.bbq_tools import OutlierFilterOpt
from omc3.tune_analysis.constants import get_kick_out_name, get_action_err_col, get_action_col, get_natq_corr_col, \
    get_corr_natq_err_col, CORRECTED, COEFFICIENT
from omc3.plotting.utils import colors as pcolors, annotations as pannot, style as pstyle
from omc3.tune_analysis.kick_file_modifiers import read_timed_dataframe
from omc3.plotting.plot_amplitude_detuning import _plot_2d
from redo_all_from_scratch import column_name, err_column_name
from tfs.tools import significant_digits
from utilities.lhc_simulation import get_detuning_from_ptc_output
from omc3.utils import logging_tools

LOGGER = logging_tools.get_logger(__name__)


MEASUREMENT_DATA = Path("/afs/cern.ch/work/j/josch/temp.bbgui_output/2022-06-24_md6863")
SIMULATION_DATA = Path("md6863_with_ip5_inj_tunes")
SIMULATION_ID = "X10X01Y01_full_and_ip5pm_b6_inj_tunes"

# OUTPUT_DATA = Path("test_first_order/02_90f7760_v7.0")
OUTPUT_DATA = Path("plots") / "second_order"

SO_TERMS = ["X20", "X11", "X02", "Y20", "Y11", "Y02"]

TUNE_COL = "NATQ{plane}CORR"


def get_corrected_detuning_ptc(folder: Path, beam: int, id_: str, columns: Sequence[str]) -> pd.Series:
    """ Get the detuning terms from the PTC difference between id_ and nominal.
    If the state of the machine at "id_" is with errors,
    this returns the detuning as INTROCUDED by the errors.
    On the other hand, if this is a machine with corrections, this is
    still the detuning introduced by the correction, i.e. the opposite sign  of
    what is compensated.
    """
    df = tfs.read(folder / f"b{beam}" / f"ampdet.lhc.b{beam}.{id_}.tfs")
    df_nominal = tfs.read(folder / f"b{beam}" / f"ampdet.lhc.b{beam}.nominal.tfs")
    detuning = pd.Series(get_detuning_from_ptc_output(df, log=False, terms=columns))
    detuning_nominal = pd.Series(get_detuning_from_ptc_output(df_nominal, log=False, terms=columns))
    return detuning - detuning_nominal


def calculate_detuning(terms: pd.Series, action: Tuple[float, float], plane: str, acd: bool = True):
    detuning = 0
    for term in terms.index:
        if term[0] not in plane:
            continue

        # detuning derivative term
        exp_x, exp_y = int(term[1]), int(term[2])
        det = terms[term] * (action[0]**exp_x) * (action[1]**exp_y)

        # coefficient (Taylor)
        if exp_x == 2 or exp_y == 2:
            det = det * 0.5

        # AC-Dipole influence
        if acd:
            if exp_x == 1 and exp_y == 1:
                det = det * 2

            if term[0].upper() == "X" and exp_x == 2:
                det = det * 3

            if term[0].upper() == "Y" and exp_y == 2:
                det = det * 3

        LOGGER.info(f"{term}: {det:.1e}")

        detuning = detuning + det
    LOGGER.info(f"Total: {detuning:.1e}")
    return detuning


def subtract_second_order_detuning(kick_df: tfs.TfsDataFrame, detuning_terms: pd.Series):
    kick_df = tfs.TfsDataFrame(kick_df.to_dict())

    col_jx = get_action_col("X")
    col_jy = get_action_col("Y")

    for tune_plane in "XY":
        col_q = get_natq_corr_col(tune_plane)
        # col_qerr = get_corr_natq_err_col(plane)
        # col_j = get_action_col(plane)
        # col_jerr = get_action_err_col(plane)

        plane_terms = detuning_terms.loc[detuning_terms.index.str.upper().str.startswith(tune_plane)]

        for kick in kick_df.index:
            action = (kick_df.loc[kick, col_jx], kick_df.loc[kick, col_jy])
            detuning = calculate_detuning(
                terms=plane_terms,
                action=action,
                plane=tune_plane,
                acd=True
            )
            kick_df.loc[kick, col_q] = kick_df.loc[kick, col_q] - detuning
    return kick_df



def main(analysis: Path):
    beam = int(re.search("(^|_)B(\d)_", analysis.name).group(2))
    kick_plane = {"H": "X", "V": "Y"}[re.search("_([HV])_", analysis.name).group(1)]

    if isinstance(analysis, str) or not analysis.is_absolute():
        analysis = MEASUREMENT_DATA / f"LHCB{beam}" / "Results" / analysis
    output = OUTPUT_DATA / analysis.name
    output.parent.mkdir(exist_ok=True, parents=True)
        
    # get detuning ptc (opposite sign, as this is what is introduced by correction)
    detuning_terms = -get_corrected_detuning_ptc(
        folder=SIMULATION_DATA,
        beam=beam if beam == 1 else 4,
        id_=SIMULATION_ID,
        columns=SO_TERMS,
    )

    # load kick_ampdet.xy file
    kick_df = read_timed_dataframe(analysis / get_kick_out_name())

    # calculate expected tune change from second order
    kick_subtracted = subtract_second_order_detuning(kick_df, detuning_terms)

    # analyse kick-data file
    kick_analysed = single_action_analysis(kick_subtracted, kick_plane, detuning_order=1, corrected=True)

    kick_so_fit = tfs.TfsDataFrame(kick_df.to_dict())
    kick_so_fit = single_action_analysis(kick_so_fit, kick_plane, detuning_order=2, corrected=True)


    opt = DotDict(
        plane=kick_plane,
        kicks=[kick_df, kick_analysed, kick_so_fit],
        labels=["default", "2nd order subtracted", "2nd order fit"],
        tune_scale=-3,
        detuning_order=2,
        action_unit="m",
        action_plot_unit="um",
        x_lim=[0, 0.016],
        y_lim=[-1.5, 1.5],
        correct_acd=True,
        output=output,
        bbq_corrected=(True,)
    )

    manual_style = {
        # "font.size": 22.5,
        # "figure.constrained_layout.use": False,
        "figure.figsize": [10.00, 4.50],
        "lines.linestyle": "none",
        "figure.subplot.left": 0.12,
        "figure.subplot.bottom": 0.15,
        "figure.subplot.right": 0.99,
        "figure.subplot.top": 0.77,
    }
    pstyle.set_style("standard", manual_style)

    for tune_plane in "XY":
        figs = _plot_2d(tune_plane, opt)
    # plt.show()
    # exit()
    for fig in figs.values():
        plt.close(fig)
    return kick_df, kick_analysed, kick_so_fit


def do_all_plots_and_subtractions():
    ALL_RESULTS = list((MEASUREMENT_DATA / "LHCB1"/ "Results").glob("*")) + list((MEASUREMENT_DATA / "LHCB2" / "Results").glob("*"))
    # ALL_RESULTS = list((MEASUREMENT_DATA / "LHCB2"/ "Results").glob("*"))
    # print("\n".join(str(p) for p in ALL_RESULTS))
    summary_df = tfs.TfsDataFrame(index=[r.name for r in ALL_RESULTS])
    for result in ALL_RESULTS:
        kick_df, kick_df_sub, kick_df_so = main(result)
        for coeff in kick_df_so.headers.keys():
            # if not coeff.endswith(f"_{CORRECTED}{COEFFICIENT.format(order=1)}"):
            if CORRECTED not in coeff or coeff.endswith("0"):
                continue

            try:
                summary_df.loc[result.name, coeff] = kick_df.headers[coeff]
            except KeyError:
                pass

            try:
                summary_df.loc[result.name, f"sub_{coeff}"] = kick_df_sub.headers[coeff]
            except KeyError:
                pass
            summary_df.loc[result.name, f"so_{coeff}"] = kick_df_so.headers[coeff]
    tfs.write(OUTPUT_DATA / "summary.tfs", summary_df.fillna(0), save_index="Result")


def redo_full_analysis():
    OUTPUT_DATA.mkdir(exist_ok=True, parents=True)
    ALL_RESULTS = list((MEASUREMENT_DATA / "LHCB1"/ "Results").glob("*")) + list((MEASUREMENT_DATA / "LHCB2" / "Results").glob("*"))
    # ALL_RESULTS = list((MEASUREMENT_DATA / "LHCB2"/ "Results").glob("*"))
    # print("\n".join(str(p) for p in ALL_RESULTS))
    summary_df = tfs.TfsDataFrame(index=[r.name for r in ALL_RESULTS])
    for analysis in ALL_RESULTS:
        beam = int(re.search("(^|_)B(\d)_", analysis.name).group(2))
        kick_plane = {"H": "X", "V": "Y"}[re.search("_([HV])_", analysis.name).group(1)]
        # kick_df = read_timed_dataframe(analysis / get_kick_out_name())
        kick_df, _ = get_kick_and_bbq_df(kick=analysis, bbq_in=analysis / "bbq_ampdet.tfs",
                                         beam=beam,
                                         filter_opt=OutlierFilterOpt(window=100, limit=0.),
                                         )

        # analyse kick-data file
        kick_analysed = single_action_analysis(kick_df, kick_plane, detuning_order=1, corrected=True)
        for coeff in kick_analysed.headers.keys():
            # if not coeff.endswith(f"_{CORRECTED}{COEFFICIENT.format(order=1)}"):
            if CORRECTED not in coeff or coeff.endswith("0"):
                continue

            try:
                summary_df.loc[analysis.name, coeff] = kick_df.headers[coeff]
            except KeyError:
                pass
    summary_df = merge_summary_data(summary_df.fillna(0))
    tfs.write(OUTPUT_DATA / "analysis_redo.tfs", summary_df, save_index="Result")


def write_merged_summary_data():
    summary_df = tfs.read(OUTPUT_DATA / "summary.tfs", index="Result")
    summary_merged_df = merge_summary_data(summary_df)
    tfs.write(OUTPUT_DATA / "summary_merged.tfs", summary_merged_df, save_index="Result")


def merge_summary_data(summary_df):
    summary_merged_df = tfs.TfsDataFrame(columns=summary_df.columns)
    for name in summary_df.index:
        if "_H_" in name:
            continue

        new_name = name.replace("AmpDet_V_", "").replace("2022-06-24_", "")
        hor_name = name.replace("_V_", "_H_")
        v_data, h_data = summary_df.loc[name, :], summary_df.loc[hor_name, :]

        assert all(np.logical_xor(v_data, h_data))
        summary_merged_df.loc[new_name.upper(), :] = v_data + h_data
    return summary_merged_df


# LATEX Printing ---------------------------------------------------------------


def latex_column(tune, action):
    return f"$Q_{{{tune.lower()},{action.lower()}}}$"


def print_latex_table(prefix: str = ""):
    df = tfs.read(OUTPUT_DATA / "summary_merged.tfs", index="Result")
    labels = {
        "1_OFF_5_OFF_b6_out":   ("2022 MD6863 w/o $b_6$", "flat-orbit"),
        "1_-160_5_+160_b6_out": ("2022 MD6863 w/o $b_6$", r"IP1\&5 xing @ $\mp$\qty{160}{\micro\radian}"),
        "1_Off_5_+160_b6_out":  ("2022 MD6863 w/o $b_6$", r"IP5 xing @ $+$\qty{160}{\micro\radian}"),
        "1_Off_5_-160_b6_out":  ("2022 MD6863 w/o $b_6$", r"IP5 xing @ $-$\qty{160}{\micro\radian}"),
        "1_-160_5_+160_b6_in":  ("2022 MD6863 w/ $b_6$",  r"IP1\&5 xing @ $\mp$\qty{160}{\micro\radian}"),
    }

    column_order = (("X", "X"), ("Y", "X"), ("X", "Y"), ("Y", "Y"))
    n_first_order = len(column_order)
    n_second_order = 0
    if prefix == "so_":
        column_order = (*column_order, ("X", "XX"), ("X", "YY"), ("Y", "XX"), ("Y", "YY"))
        n_second_order = len(column_order) - n_first_order

    latex_str = []
    # HEADER
    latex_str.append(" "*45 + " & " + " & ".join(f"{latex_column(tune, action):21s}" for tune, action in column_order) + r"\\")
    latex_str.append(" "*46 + n_first_order * r"& \unitcellfo           " + n_second_order * r"& \unitcellso           " + r"\\")
    latex_str.append(r"\midrule[1pt]")

    # Entries
    for meas, label in labels.items():
        for idx, beam in enumerate((1, 2)):
            beam_label = f"B{beam}_{meas}"
            beam_str = "\\bblue" if beam == 1 else "\\bred"
            entries = []
            for tune, action in column_order:
                scale = 1e-3 if len(action) == 1 else 1e-11
                acd_scale = 1
                if len(action) == 1 and tune == action:
                    acd_scale = 0.5
                if len(action) == 2 and tune == action[0]:
                    acd_scale = 1/3
                value_col, error_col = f"{prefix}{column_name(tune, action)}", f"{prefix}{err_column_name(tune, action)}"
                value, error = df.loc[beam_label, value_col], df.loc[beam_label, error_col]
                value_str, error_str = significant_digits(value*scale*acd_scale, error*scale*acd_scale)
                # entries.append(f'{f"{value_str}({error_str})":10s}')
                entries.append(f'{f"{beam_str}{{{value_str}}}{{{error_str}}}":20s}')

            latex_str.append(f"{label[idx]:45s} & " + "  & ".join(entries) + r"\\")
        latex_str.append("\midrule")

    print("\n".join(latex_str))


def print_aaaaaa(prefix: str = ""):
    df = tfs.read(OUTPUT_DATA / "summary_merged.tfs", index="Result")
    labels = {
        "1_OFF_5_OFF_b6_out":   ("2022 MD6863 w/o $b_6$", "flat-orbit"),
        "1_-160_5_+160_b6_out": ("2022 MD6863 w/o $b_6$", r"IP1\&5 xing @ $\mp$\qty{160}{\micro\radian}"),
        "1_Off_5_+160_b6_out":  ("2022 MD6863 w/o $b_6$", r"IP5 xing @ $+$\qty{160}{\micro\radian}"),
        "1_Off_5_-160_b6_out":  ("2022 MD6863 w/o $b_6$", r"IP5 xing @ $-$\qty{160}{\micro\radian}"),
        "1_-160_5_+160_b6_in":  ("2022 MD6863 w/ $b_6$",  r"IP1\&5 xing @ $\mp$\qty{160}{\micro\radian}"),
    }

    column_order = (("X", "X"), ("Y", "X"), ("X", "Y"), ("Y", "Y"))
    n_first_order = len(column_order)
    n_second_order = 0
    if prefix == "so_":
        column_order = (*column_order, ("X", "XX"), ("X", "YY"), ("Y", "XX"), ("Y", "YY"))
        n_second_order = len(column_order) - n_first_order

    latex_str = []
    # HEADER
    latex_str.append(" "*45 + " & " + " & ".join(f"{latex_column(tune, action):21s}" for tune, action in column_order) + r"\\")
    latex_str.append(" "*46 + n_first_order * r"& \unitcellfo           " + n_second_order * r"& \unitcellso           " + r"\\")
    latex_str.append(r"\midrule[1pt]")

    # Entries
    for meas, label in labels.items():
        for idx, beam in enumerate((1, 2)):
            beam_label = f"B{beam}_{meas}"
            beam_str = "\\bblue" if beam == 1 else "\\bred"
            entries = []
            for tune, action in column_order:
                scale = 1e-3 if len(action) == 1 else 1e-11
                acd_scale = 1
                if len(action) == 1 and tune == action:
                    acd_scale = 0.5
                if len(action) == 2 and tune == action[0]:
                    acd_scale = 1/3
                value_col, error_col = f"{prefix}{column_name(tune, action)}", f"{prefix}{err_column_name(tune, action)}"
                value, error = df.loc[beam_label, value_col], df.loc[beam_label, error_col]
                value_str, error_str = significant_digits(value*scale*acd_scale, error*scale*acd_scale)
                # entries.append(f'{f"{value_str}({error_str})":10s}')
                entries.append(f'{f"{beam_str}{{{value_str}}}{{{error_str}}}":20s}')

            latex_str.append(f"{label[idx]:45s} & " + "  & ".join(entries) + r"\\")
        latex_str.append("\midrule")

    print("\n".join(latex_str))

def plot_all_data():
    labels = {
        "1_OFF_5_OFF_b6_out":   ("2022 MD6863 w/o $b_6$", "flat-orbit"),
        "1_-160_5_+160_b6_out": ("2022 MD6863 w/o $b_6$", r"IP1\&5 xing @ $\mp$\qty{160}{\micro\radian}"),
        "1_Off_5_+160_b6_out":  ("2022 MD6863 w/o $b_6$", r"IP5 xing @ $+$\qty{160}{\micro\radian}"),
        "1_Off_5_-160_b6_out":  ("2022 MD6863 w/o $b_6$", r"IP5 xing @ $-$\qty{160}{\micro\radian}"),
    }


if __name__ == '__main__':
    redo_full_analysis()
    # do_all_plots_and_subtractions()
    # write_merged_summary_data()
    # print_latex_table()
    # print()
    # print_latex_table(prefix="sub_")
    # print()
    # print_latex_table(prefix="so_")
