"""
MD6863 - check the influence of Landau Powering
--------------------------------------------------------

"""
from pathlib import Path

import pandas as pd

import tfs
from correction_via_amplitude_detuning_multi_xing import (
    create_optics, check_corrections
)
from utilities.utils import log_setup
from cpymad_lhc.general import power_landau_octupoles
from utilities.lhc_simulation import get_detuning_from_ptc_output

import logging

LOGGER = logging.getLogger(__name__)


def get_sum(meas_a, meas_b):
    return {beam: meas_a[beam] + meas_b[beam] for beam in meas_a.keys()}


def get_diff(meas_a, meas_b):
    return {beam: meas_a[beam] - meas_b[beam] for beam in meas_a.keys()}


def to_str(s: pd.Series):
    return ", ".join(f"{k}: {v: 6.2f}" for k, v in s.to_dict().items())


def get_detuning(beam, scenario):
    return pd.Series(get_detuning_from_ptc_output(tfs.read_tfs(output / f"full_b{beam}" / f"ampdet.lhc.b{beam}.{scenario}.tfs")))


# Define Machine Data
# -------------------

# Output Path
output = Path("md6863_check_landau")

# Fill in special values for crossing if needed:
xings = {
    'full': {'scheme': 'flat', 'on_x1': -160, 'on_x5': 160},
}


def simulation():
    paths = {i: output / f"b{i}" for i in (1, 4)}
    lhc_beams = create_optics(paths, xings=xings, year=2022, tune_x=62.28, tune_y=60.31)
    for beams in lhc_beams.values():
        for lhcbeam in beams.values():
            lhcbeam.reinstate_loggers()
            power_landau_octupoles(lhcbeam.madx, mo_current=570, beam=lhcbeam.beam, defective_arc=False)
            lhcbeam.match_tune()
            lhcbeam.get_ampdet("mo_570")

            power_landau_octupoles(lhcbeam.madx, mo_current=570, beam=lhcbeam.beam, defective_arc=True)
            lhcbeam.match_tune()
            lhcbeam.get_ampdet("mo_570_defective_arc")

            power_landau_octupoles(lhcbeam.madx, mo_current=440, beam=lhcbeam.beam, defective_arc=False)
            lhcbeam.match_tune()
            lhcbeam.get_ampdet("mo_440")

            power_landau_octupoles(lhcbeam.madx, mo_current=440, beam=lhcbeam.beam, defective_arc=True)
            lhcbeam.match_tune()
            lhcbeam.get_ampdet("mo_440_defective_arc")
            lhcbeam.madx.exit()


def evaluate():
    for beam in (1, 4):
        print(f"Beam {beam}")
        nominal = get_detuning(beam, "nominal")
        for scenario in ("mo_570", "mo_570_defective_arc", "mo_440", "mo_440_defective_arc"):
            ampdet = get_detuning(beam, scenario)
            diff = ampdet - nominal
            print(f"{scenario:21s}: {to_str(diff*1e-3)}")


if __name__ == '__main__':
    # log_setup()
    # simulation()
    evaluate()
