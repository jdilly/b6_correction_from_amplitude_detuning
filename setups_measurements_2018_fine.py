from pathlib import Path

from correction_via_amplitude_detuning import main, calculate_from_prerun_optics
from classes import Detuning, Constraints, TargetData, Target
from plotting import ltx_dqd2j, plot_detuning_ips, plot_correctors


def ip5_ip1_fine_tuning():
    xing = {'scheme': 'top'}
    targets = [
        Target(
            name="X10X01Y01_0",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=0, Y01=6),
                    2: Detuning(X10=9, X01=0, Y01=6),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=0, Y01=-41),
                        2: Detuning(X10=-4.5, X01=0, Y01=1),
                    }),
            ]
        ),
        Target(
            name="X10X01Y01_25",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=25, Y01=6),
                    2: Detuning(X10=9, X01=25, Y01=6),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=25, Y01=-41),
                        2: Detuning(X10=-4.5, X01=25, Y01=1),
                    }),
            ]
        ),
        Target(
            name="X10X01Y01_025",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=0, Y01=6),
                    2: Detuning(X10=9, X01=25, Y01=6),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=0, Y01=-41),
                        2: Detuning(X10=-4.5, X01=25, Y01=1),
                    }),
            ]
        ),
        Target(
            name="X10X01Y01_25_noY01b2",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=25, Y01=6),
                    2: Detuning(X10=9, X01=25),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=25, Y01=-41),
                        2: Detuning(X10=-4.5, X01=25),
                    }),
            ]
        ),
        Target(
            name="X10X01Y01_025_noY01b2",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=0, Y01=6),
                    2: Detuning(X10=9, X01=25),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=0, Y01=-41),
                        2: Detuning(X10=-4.5, X01=25),
                    }),
            ]
        ),
        Target(
            name="X10Y01_ip5_ip1_constrX01",
            data=[
                TargetData(
                    ips=5,
                    detuning={
                        1: Detuning(X10=55.2,  Y01=6),
                        2: Detuning(X10=9, Y01=6),
                    },
                    constraints={
                        1: Constraints(X01=">=0"),
                        2: Constraints(X01=">=0"),
                    }
                ),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, Y01=-41),
                        2: Detuning(X10=-4.5, Y01=1),
                    },
                    constraints={
                        1: Constraints(X01=">=0"),
                        2: Constraints(X01=">=0"),
                    }
                ),
            ]
        ),
        Target(
            name="X10Y01_ip15_constrX01",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: Detuning(X10=33.2, Y01=-35),
                    2: Detuning(X10=4.5, Y01=7),
                },
                constraints={
                    1: Constraints(X01=">=0"),
                    2: Constraints(X01=">=0"),
                }
            ),
        ),
        Target(
            name="X10Y01_ip5_ip1_constrX01_noY01b2",
            data=[
                TargetData(
                    ips=5,
                    detuning={
                        1: Detuning(X10=55.2,  Y01=6),
                        2: Detuning(X10=9),
                    },
                    constraints={
                        1: Constraints(X01=">=0"),
                        2: Constraints(X01=">=0"),
                    }
                ),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, Y01=-41),
                        2: Detuning(X10=-4.5),
                    },
                    constraints={
                        1: Constraints(X01=">=0"),
                        2: Constraints(X01=">=0"),
                    }
                ),
            ]
        ),
        Target(
            name="X10Y01_multi_ip",
            data=[
                TargetData(
                    ips=5,
                    detuning={
                        1: Detuning(X10=55.2,  Y01=6),
                        2: Detuning(X10=9, Y01=6),
                    },
                    constraints={
                        1: Constraints(X01=">=-10"),
                        2: Constraints(X01=">=-10"),
                    }
                ),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, Y01=-41),
                        2: Detuning(X10=-4.5),
                    },
                    constraints={
                        1: Constraints(X01=">=-10"),
                        2: Constraints(X01=">=-10"),
                    }
                ),
                TargetData(
                    ips=(1, 5,),
                    detuning={
                        1: Detuning(X10=33.2, Y01=-35),
                        2: Detuning(X10=4.5),
                    },
                    constraints={
                        1: Constraints(X01=">=0"),
                        2: Constraints(X01=">=0", Y01=">=0"),
                    }
                ),
            ]
        ),
        # Target(
        #     name="X10Y01_multi_ip_approxX10ip5",
        #     data=[
        #         TargetData(
        #             ips=5,
        #             detuning={
        #                 1: Detuning(Y01=6),
        #                 2: Detuning(X10=9, Y01=6),
        #             },
        #             constraints={
        #                 1: Constraints(X10=">=50", X01=">=-20"),
        #                 2: Constraints(X01=">=-20"),
        #             }
        #         ),
        #         TargetData(
        #             ips=1,
        #             detuning={
        #                 1: Detuning(X10=-22, Y01=-41),
        #                 2: Detuning(X10=-4.5),
        #             },
        #             constraints={
        #                 1: Constraints(X01=">=0"),
        #                 2: Constraints(X01=">=0"),
        #             }
        #         ),
        #         TargetData(
        #             ips=(1, 5,),
        #             detuning={
        #                 1: Detuning(X10=33.2, Y01=-35),
        #                 2: Detuning(X10=4.5),
        #             },
        #             constraints={
        #                 1: Constraints(X01=">=0"),
        #                 2: Constraints(X01=">=0", Y01=">=0"),
        #             }
        #         ),
        #     ]
        # ),
    ]
    paths = {i: Path(f"nominal_xing_fine_tune_b1b4/b{i}") for i in (1, 4)}
    main(paths, xing=xing, targets=targets)
    # calculate_from_prerun_optics(inputdirs=paths,
    #                              outputdirs={i: Path(f"nominal_xing_fine_tune_b1b4_calc/b{i}") for i in (1, 4)},
    #                              targets=targets)


def plot_xing_fine(fields):
    path = Path('nominal_xing_fine_tune_b1b4')
    ids = [f"{s}_{fields}" for s in ('X10X01Y01_0', 'X10X01Y01_25', 'X10X01Y01_025', 'X10X01Y01_25_noY01b2', 'X10X01Y01_025_noY01b2',
                                     )]

    nchar = 10
    labels=[
        f"${ltx_dqd2j('x', 'x')}$ = {'55.2-22 | 9-4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ = {'0+0'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | 6+1'.center(nchar)}",
        f"${ltx_dqd2j('x', 'x')}$ = {'55.2-22 | 9-4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ = {'25+25'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | 6+1'.center(nchar)}",
        f"${ltx_dqd2j('x', 'x')}$ = {'55.2-22 | 9-4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ = {'0+0 | 25+25'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | 6+1'.center(nchar)}",
        f"${ltx_dqd2j('x', 'x')}$ = {'55.2-22 | 9-4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ = {'25+25'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | --'.center(nchar)}",
        f"${ltx_dqd2j('x', 'x')}$ = {'55.2-22 | 9-4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ = {'0+0 | 25+25'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | --'.center(nchar)}",
    ]
    output_id = f"_{fields}_correction"
    plot_detuning_ips(
        path,
        ids=ids,
        labels=labels,
        fields=fields,
        size=[12., 5.],
        measurement={1: {"X10": [33.2, 1.12], "Y01": [-35, 1.41]}, 4: {"X10": [4.5, 1.12], "Y01": [7, 3.2]}},
        beams=(1, 4),
        ylims={1: [-62, 62], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id
    )
    corr_size = [6.00, 6.90]
    if 'b5' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr][15]', order="5", output_id=f'{output_id}_b5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr]5', order="5", output_id=f'{output_id}_b5_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr]1', order="5", output_id=f'{output_id}_b5_ip1')

    if 'b6' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=output_id)
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}_b6_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}_b6_ip1')


def plot_xing_fine_constr(fields):
    path = Path('nominal_xing_fine_tune_b1b4')
    ids = [f"{s}_{fields}" for s in (
        'X10Y01_ip5_ip1_constrX01', 'X10Y01_ip15_constrX01',"X10Y01_ip5_ip1_constrX01_noY01b2",
        "X10Y01_multi_ip",
        # "X10Y01_multi_ip_approxX10ip5"
    )]

    nchar = 10
    labels=[
        f"${ltx_dqd2j('x', 'x')}$ = {'55.2-22 | 9-4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ >= {'0,0'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | 6+1'.center(nchar)}",
        f"${ltx_dqd2j('x', 'x')}$ = {'33.2 | 4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ >= {'0'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-35 | 7'.center(nchar)}",
        f"${ltx_dqd2j('x', 'x')}$ = {'55.2-22 | 9-4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ >= {'0,0'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | --'.center(nchar)}",
        f"${ltx_dqd2j('x', 'x')}$ = {'55.2-22 | 9-4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ >= {'-10,-10'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | 6 + --'.center(nchar)}\n${ltx_dqd2j('x', 'x')}$ = {'33.2 | 4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ >= {'0'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-35 | >= 0'.center(nchar)}",
        # f"${ltx_dqd2j('x', 'x')}$ = {'>=50,-22 | 9-4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ >= {'-10,-10'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | 6 + --'.center(nchar)}\n${ltx_dqd2j('x', 'x')}$ = {'33.2 | 4.5'.center(nchar)}\n${ltx_dqd2j('x', 'y')}$ >= {'0'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-35 | >= 0'.center(nchar)}",
    ]
    output_id = f"_{fields}_correction_constr"
    plot_detuning_ips(
        path,
        ids=ids,
        labels=labels,
        fields=fields,
        size=[12., 6.],
        measurement={1: {"X10": [33.2, 1.12], "Y01": [-35, 1.41]}, 4: {"X10": [4.5, 1.12], "Y01": [7, 3.2]}},
        beams=(1, 4),
        ylims={1: [-62, 62], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id
    )
    corr_size = [6.00, 6.90]
    if 'b5' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr][15]', order="5", output_id=f'{output_id}_b5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr]5', order="5", output_id=f'{output_id}_b5_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr]1', order="5", output_id=f'{output_id}_b5_ip1')

    if 'b6' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=output_id)
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}_b6_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}_b6_ip1')
