"""
Plot a comparison between the K6L from the corrections and
from simulations using WISE (from Thomas).
"""

import re
from pathlib import Path
from typing import Dict

import matplotlib as mpl
import matplotlib.patheffects as path_effects
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import pandas as pd
from matplotlib.patches import Ellipse

import tfs
from matplotlib.lines import Line2D

from correction_via_amplitude_detuning import LENGTH_MCTX
from omc3.plotting.utils import style as pstyle, annotations as pannot, colors as pcolors, lines as plines
from omc3.utils import logging_tools
from utilities.classes import MeasureValue

LOGGER = logging_tools.get_logger(__name__)

N_SEEDS = 60

WISE_DATA = Path('/') / 'afs' / 'cern.ch' / 'work' / 't' / 'thpugnat' / 'public' / '2017_topenergy'
JDILLY_DATA = Path('/') / 'afs' / 'cern.ch' / 'work' / 'j' / 'jdilly4' / 'study.22.wise_b6_correction'
JDILLY_DATA_HEADS = Path('/') / 'afs' / 'cern.ch' / 'work' / 'j' / 'jdilly4' / 'study.22.wise_heads_b6_correction'

MAGNETS = ["MCTX.3L1", "MCTX.3R1", "MCTX.3L5", "MCTX.3R5"]


OUTPUT = Path(__file__).parent / "wise_compare"


def circuit_to_magnet_name(circuit: str):
    m = re.match(r"K(?P<name>\w+)(?P<position>\d+)\.(?P<suffix>.+)", circuit.upper())
    return f"M{m.group('name')}.{m.group('position')}{m.group('suffix')}"


def gather_wise_data_thomas():
    name_map = {"he": "WISEnom", 'he_heads': "WISEric"}

    for name, mapped in name_map.items():
        path = WISE_DATA / f"Res_{mapped}"
        df_name = f"correctors_{mapped}.dat"
        df = pd.DataFrame(index=range(1, N_SEEDS+1), columns=MAGNETS)

        for ii in range(1, N_SEEDS+1):
            df_seed = tfs.read(path / f"output_{ii}" / df_name, index="NAME")
            df.loc[ii, :] = df_seed.loc[MAGNETS, "K5L"].transpose()

        tfs.write(OUTPUT / f"df_wise_{name}.tfs", df, save_index="SEED")


def gather_wise_data_jdilly(heads=False):
    # for wise in (2011, 2015, 2021):
    data_dir = JDILLY_DATA_HEADS if heads else JDILLY_DATA
    heads_str = "_heads" if heads else ""

    for wise in (2021,):
        n_seeds = 100 if wise == 2021 else 60
        df = pd.DataFrame(index=range(1, n_seeds+1), columns=MAGNETS)

        for seed in range(1, n_seeds+1):
            path = data_dir / f"Job.wise{wise}_{seed}" / "Outputdata" / "settings.lhc.b1.mcx_b1.tfs"
            LOGGER.info(f"Loading file: {path}")

            df_seed = tfs.read(path, index="name")
            df.loc[seed, :] = df_seed.loc[MAGNETS, "value"].transpose()

        tfs.write(OUTPUT / f"df_wise{heads_str}_jdilly{wise}.tfs", df, save_index="SEED")


def read_settings_data(settings_path: Path):
    try:
        df = tfs.read(settings_path, index="CIRCUIT")
    except tfs.errors.TfsFormatError:
        df = pd.DataFrame(index=MAGNETS, columns = ["VALUE", "KNL"])
        pattern = r'([^\s]+)\s+:=\s*([\d.+-]+)[^;]+;[^=]+=\s*([\d.+-]+)'
        for line in settings_path.read_text().split('\n'):
            if line.strip().startswith("!"):
                continue
            match = re.match(pattern, line.strip())
            magnet = circuit_to_magnet_name(match.group(1))
            df.loc[magnet, "KNL"] = float(match.group(2))
            df.loc[magnet, "VALUE"] = float(match.group(3))
    else:
        df.index = [circuit_to_magnet_name(s) for s in df.index]
    return df


def get_first_file(path):
    settings_path = path.glob("settings.*.tfs")
    if not settings_path:
        settings_path = path.glob("settings.*.madx")
    if not settings_path:
        raise IOError(f"No settings found in {path}")
    return settings_path[0]


def gather_correction_data(input_dirs: Dict[Path, str]):
    df_corrections = pd.DataFrame(index=input_dirs.keys(), columns=MAGNETS)
    for path, id_ in input_dirs.items():
        if id_ is None:
            settings_path = get_first_file(path)
        else:
            settings_path = path / f'settings.{id_}.tfs'
            if not settings_path.exists():
                settings_path = settings_path.with_suffix('.madx')
        df_corrections.loc[path, :] = read_settings_data(settings_path)["KNL"].transpose()
    return df_corrections


def plot_data():

    iteration_map = {
       "2011": {"he": "WISE 2011, Optics 2017 40cm", 'he_heads': "WISE 2011 + Heads, Optics 2017 40cm", "jdilly2011": "WISE 2011, Optics 2018 30cm"},
       "2011_2021": {"he": "WISE 2011", 'he_heads': "WISE 2011 + Heads", "jdilly2021": "WISE 2021", "heads_jdilly2021": "WISE 2021 + Heads"},
       "2015_2021": {"jdilly2011": "WISE 2011", "jdilly2015": "WISE 2015", "jdilly2021": "WISE 2021"},
    }

    magnets = ["MCTX.3L", "MCTX.3R"]
    markers = {
        # 1: plines.text_to_marker(str(1)),
        # 5: plines.text_to_marker(str(5)),
        1: "$1$",
        5: "$5$",
    }
    for idx_plot, (out_name, name_map) in enumerate(iteration_map.items()):
        fig, ax = plt.subplots()
        ax.set_aspect('equal', adjustable='box')
        scale = 1e-3

        n_entries = len(name_map)
        handles = [None] * n_entries
        labels = [None] * n_entries

        for idx, (name, label) in enumerate(name_map.items()):
            df = tfs.read(OUTPUT / f"df_wise_{name}.tfs", index="SEED")
            color = pcolors.get_mpl_color(idx)
            for ip in (1, 5):
                for seed in range(1, N_SEEDS+1):
                    txt = ax.text(
                        df.loc[seed, f'{magnets[0]}{ip}']*scale,
                        df.loc[seed, f'{magnets[1]}{ip}']*scale,
                        s=str(ip),
                        color="none",
                        ha='center',
                        va='center',
                        fontsize=mpl.rcParams['lines.markersize'],
                        fontdict=dict(weight="bold"),
                        )
                    txt.set_path_effects([
                        path_effects.PathPatchEffect(
                            edgecolor=color,
                            linewidth=0.3*mpl.rcParams['lines.linewidth'],
                            facecolor="white")]
                    )
                labels[idx] = label
                handles[idx] = Line2D([0], [0], marker=plines.text_to_marker(f"1,5"),
                                               markeredgecolor=color,
                                               markerfacecolor="white",
                                               markersize=25,
                                               ls='')

        label = r"$K_6L$ {0}  $\left[10^3 \mathrm{{m}}^{{-5}}\right]$"
        ax.set_xlabel(label.format("Left"))
        ax.set_ylabel(label.format("Right"))

        ax.set_xlim([-1.5, 6])
        ax.set_ylim([-5, 0])
        ax.xaxis.set_major_locator(mticker.MultipleLocator(1))
        ax.xaxis.set_minor_locator(mticker.MultipleLocator(0.1))
        ax.yaxis.set_major_locator(mticker.MultipleLocator(1))
        ax.yaxis.set_minor_locator(mticker.MultipleLocator(0.1))

        # ncol = 1 if any(len(n)>20 for n in name_map.values()) else 2
        ncol = idx_plot + 1

        pannot.make_top_legend(ax, ncol=ncol, handles=handles, labels=labels, transposed=True)
        plt.show()
        fig.savefig(OUTPUT / f"plot.wise_comparison_{out_name}.pdf")



def plot_data_2011():
    name_map = {"he": "Thomas", "jdilly2011": "Josch"}


    magnets = ["MCTX.3L", "MCTX.3R"]
    markers = {
        # 1: plines.text_to_marker(str(1)),
        # 5: plines.text_to_marker(str(5)),
        1: "$1$",
        5: "$5$",
    }

    fig, ax = plt.subplots()
    ax.set_aspect('equal', adjustable='box')
    scale = 1e-3

    n_entries = len(name_map)
    handles = [None] * n_entries
    labels = [None] * n_entries

    for idx, (name, label) in enumerate(name_map.items()):
        df = tfs.read(OUTPUT / f"df_wise_{name}.tfs", index="SEED")
        color = pcolors.get_mpl_color(idx)
        for ip in (1, 5):
            for seed in range(1, N_SEEDS+1):
                txt = ax.text(
                    df.loc[seed, f'{magnets[0]}{ip}']*scale,
                    df.loc[seed, f'{magnets[1]}{ip}']*scale,
                    s=str(ip),
                    color="none",
                    ha='center',
                    va='center',
                    fontsize=mpl.rcParams['lines.markersize'],
                    fontdict=dict(weight="bold"),
                    )
                txt.set_path_effects([
                    path_effects.PathPatchEffect(
                        edgecolor=color,
                        linewidth=0.3*mpl.rcParams['lines.linewidth'],
                        facecolor="white")]
                )
            labels[idx] = label
            handles[idx] = Line2D([0], [0], marker=plines.text_to_marker(f"1,5"),
                                  markeredgecolor=color,
                                  markerfacecolor="white",
                                  markersize=25,
                                  ls='')

    label = r"$K_6L$ {0}  $\left[10^3 \mathrm{{m}}^{{-5}}\right]$"
    ax.set_xlabel(label.format("Left"))
    ax.set_ylabel(label.format("Right"))

    # legend = pannot.make_top_legend(ax, ncol=2)
    # pmisc.square_axis_limits(ax)

    for ip in (1, 5):
        if ip == 1:
            ax.set_xlim([1.6, 1.9])
            ax.set_ylim([-3.62, -3.32])
        else:
            ax.set_xlim([4.27, 4.62])
            ax.set_ylim([-3.45, -3.1])
        #
        # ax.xaxis.set_major_locator(mticker.MultipleLocator(1))
        ax.xaxis.set_minor_locator(mticker.MultipleLocator(0.01))
        # ax.yaxis.set_major_locator(mticker.MultipleLocator(1))
        ax.yaxis.set_minor_locator(mticker.MultipleLocator(0.01))

        pannot.make_top_legend(ax, ncol=2, handles=handles, labels=labels, transposed=True)
        fig.savefig(OUTPUT / f"plot.wise2011_comparison_IP{ip}.pdf")

    plt.show()


def plot_data_beambased_vs_2022_model():
    name_map = {"heads_jdilly2021": "Simulation"}
    # name_map = {"jdilly2021": "WISE2021", "heads_jdilly2021": "WISE2021 + HEADS"}

    correction_data_map = {
        Path('commish2022/b1'): 'lhc.b1.X10X01Y01_global_b6',
        Path('md6863/b1'): 'lhc.b1.X10X01Y01_global_b6',
        # Path('md6863_with_ip5/b1'): 'lhc.b1.X10X01Y01_full_and_ip5pm_b6',
        Path('md6863_with_ip5_inj_tunes/b1'): 'lhc.b1.X10X01Y01_full_and_ip5pm_b6',
    }
    correction_name_map = {
        Path('commish2022/b1'): 'Commissioning',
        Path('md6863/b1'): 'MD6863 w/o IP5',
        # Path('md6863_with_ip5/b1'): 'MD6863 w/ IP5',
        Path('md6863_with_ip5_inj_tunes/b1'): 'MD6863 w/ IP5',
    }

    magnets = ["MCTX.3L", "MCTX.3R"]
    markers = {
        # 1: plines.text_to_marker(str(1)),
        # 5: plines.text_to_marker(str(5)),
        1: "$1$",
        5: "$5$",
    }

    fig, ax = plt.subplots()
    ax.set_aspect('equal', adjustable='box')
    scale = 1e-3


    df_correction = gather_correction_data(correction_data_map)
    n_entries = len(name_map) + len(df_correction)
    handles = [None] * n_entries
    labels = [None] * n_entries


    # for idx, (name, label) in enumerate(name_map.items()):
    #     df = tfs.read(f"df_wise_{name}.tfs", index="SEED")
    #     color = pcolors.get_mpl_color(idx)
    #     for ip in (1, 5):
    #         ax.plot(
    #             df[f'{magnets[0]}{ip}']*scale, df[f'{magnets[1]}{ip}']*scale,
    #             marker=markers[ip],
    #             markersize=mpl.rcParams['lines.markersize']*1.5,
    #             markeredgecolor=color,
    #             markerfacecolor="white",
    #             linestyle='',
    #             label=f"{label} IP{ip}",
    #             )


    for idx, (name, label) in enumerate(name_map.items()):
        df = tfs.read(OUTPUT / f"df_wise_{name}.tfs", index="SEED")
        color = pcolors.get_mpl_color(idx)
        for ip in (1, 5):
            for seed in range(1, N_SEEDS+1):
                txt = ax.text(
                    df.loc[seed, f'{magnets[0]}{ip}']*scale,
                    df.loc[seed, f'{magnets[1]}{ip}']*scale,
                    s=str(ip),
                    color="none",
                    ha='center',
                    va='center',
                    fontsize=mpl.rcParams['lines.markersize'],
                    fontdict=dict(weight="bold"),
                    )
                txt.set_path_effects([
                    path_effects.PathPatchEffect(
                        edgecolor=color,
                        linewidth=0.3*mpl.rcParams['lines.linewidth'],
                        facecolor="white")]
                )
                # # test if text is actually centered
                # ax.plot(
                #     df.loc[seed, f'{magnets[0]}{ip}'] * scale,
                #     df.loc[seed, f'{magnets[1]}{ip}'] * scale,
                #     marker="x",
                # )
                # ax.plot(
                #     df.loc[seed, f'{magnets[0]}{ip}'] * scale,
                #     df.loc[seed, f'{magnets[1]}{ip}'] * scale,
                #     marker=f"${ip}$",
                #     )
                # plt.show()
            labels[idx] = label
            handles[idx] = Line2D([0], [0], marker=plines.text_to_marker(f"1,5"),
                                  markeredgecolor=color,
                                  markerfacecolor="white",
                                  markersize=25,
                                  ls='')

    for idx, (name, label) in enumerate(correction_name_map.items(), start=len(name_map)):
        color = pcolors.get_mpl_color(idx)
        for ip in (1, 5):
            ax.scatter(
                df_correction.loc[name, f'{magnets[0]}{ip}']*scale, df_correction.loc[name, f'{magnets[1]}{ip}']*scale,
                marker=markers[ip],
                color=color,
                label=f"{label} IP{ip}",
                )
        labels[idx] = label
        handles[idx] = Line2D([0], [0], marker=plines.text_to_marker(f"1,5"),
                              color=color,
                              markersize=25,
                              ls='')

    label = r"$K_6L$ {0}  $\left[10^3 \mathrm{{m}}^{{-5}}\right]$"
    ax.set_xlabel(label.format("Left"))
    ax.set_ylabel(label.format("Right"))

    # legend = pannot.make_top_legend(ax, ncol=2)
    # pmisc.square_axis_limits(ax)

    # IP1
    # ax.set_xlim([1.6, 1.9])
    # ax.set_ylim([-3.6, -3.3])
    # IP5
    # ax.set_xlim([4.3, 4.6])
    # ax.set_ylim([-3.45, -3.1])
    #
    ax.set_xlim([-1.5, 4.2])
    ax.set_ylim([-3.5, 0.2])
    # ax.set_xlim([-10, 10])
    # ax.set_ylim([-10, 10])
    ax.xaxis.set_major_locator(mticker.MultipleLocator(1))
    ax.xaxis.set_minor_locator(mticker.MultipleLocator(0.1))
    ax.yaxis.set_major_locator(mticker.MultipleLocator(1))
    ax.yaxis.set_minor_locator(mticker.MultipleLocator(0.1))

    pannot.make_top_legend(ax, ncol=2, handles=handles, labels=labels, transposed=True)
    plt.show()
    fig.savefig(OUTPUT / f"plot.wise_comparison_paper.pdf")
    # fig.savefig(OUTPUT / f"plot.wise_comparison_2021heads.pdf")

def plot_data_beambased_vs_2022_model_with_errors():
    name_map = {"heads_jdilly2021": "Simulation"}
    # name_map = {"jdilly2021": "WISE2021", "heads_jdilly2021": "WISE2021 + HEADS"}

    correction_data_map = {
        Path('commish2022/b1'):               {"MCTX.3L1": MeasureValue(-606, 715), "MCTX.3R1": MeasureValue(-2696, 1179), "MCTX.3L5": MeasureValue(5004, 752), "MCTX.3R5": MeasureValue(-5053, 907)},
        Path('md6863/b1'):                    {"MCTX.3L1": MeasureValue(1269, 731), "MCTX.3R1": MeasureValue(-3288, 577),  "MCTX.3L5": MeasureValue(6367, 563), "MCTX.3R5": MeasureValue(-4087, 782)},
        Path('md6863_with_ip5_inj_tunes/b1'): {"MCTX.3L1": MeasureValue(385, 134),  "MCTX.3R1": MeasureValue(-4081, 175),  "MCTX.3L5": MeasureValue(5336, 114), "MCTX.3R5": MeasureValue(-5059, 121)},
    }
    correction_name_map = {
        Path('commish2022/b1'): 'Commissioning',
        Path('md6863/b1'): 'MD6863 w/o IP5',
        Path('md6863_with_ip5_inj_tunes/b1'): 'MD6863 w/ IP5',
    }

    magnets = ["MCTX.3L", "MCTX.3R"]
    markers = {
        # 1: plines.text_to_marker(str(1)),
        # 5: plines.text_to_marker(str(5)),
        1: "$1$",
        5: "$5$",
    }

    fig, ax = plt.subplots()
    ax.set_aspect('equal', adjustable='box')
    scale = 1e-3


    df_correction = pd.DataFrame(correction_data_map).transpose()
    n_entries = len(name_map) + len(df_correction)
    handles = [None] * n_entries
    labels = [None] * n_entries


    for idx, (name, label) in enumerate(name_map.items()):
        df = tfs.read(OUTPUT / f"df_wise_{name}.tfs", index="SEED")
        color = pcolors.get_mpl_color(idx)
        for ip in (1, 5):
            for seed in range(1, N_SEEDS+1):
                txt = ax.text(
                    df.loc[seed, f'{magnets[0]}{ip}']*scale,
                    df.loc[seed, f'{magnets[1]}{ip}']*scale,
                    s=str(ip),
                    color="none",
                    ha='center',
                    va='center',
                    fontsize=mpl.rcParams['lines.markersize']*1.2,
                    fontdict=dict(weight="bold"),
                    )
                txt.set_path_effects([
                    path_effects.PathPatchEffect(
                        edgecolor=color,
                        linewidth=0.3*mpl.rcParams['lines.linewidth'],
                        facecolor="white")]
                )
            labels[idx] = label
            handles[idx] = Line2D([0], [0], marker=plines.text_to_marker(f"1,5"),
                                  markeredgecolor=color,
                                  markerfacecolor="white",
                                  markersize=25,
                                  ls='')

    for idx, (name, label) in enumerate(correction_name_map.items(), start=len(name_map)):
        color = pcolors.get_mpl_color(idx)
        for ip in (1, 5):
            x = df_correction.loc[name, f'{magnets[0]}{ip}']*scale * LENGTH_MCTX
            y = df_correction.loc[name, f'{magnets[1]}{ip}']*scale * LENGTH_MCTX
            # ax.plot(
            #     x.value,
            #     y.value,
            #     # marker=markers[ip], # if "w/ " not in label else "",
            #     marker="x",
            #     markersize=mpl.rcParams['lines.markersize'], #* (1 if "w/ " not in label else 0.45),
            #     color=color,
            #     # color=(1, 1, 1),
            #     label=f"{label} IP{ip}",
            #     zorder=-1,
            #     )
            ax.plot(
                x.value - (0 if "w/ " not in label else 0.01),
                y.value,
                marker=markers[ip], # if "w/ " not in label else "",
                # marker="x",
                markersize=mpl.rcParams['lines.markersize'] * (1 if "w/ " not in label else 0.45),
                color=color,
                # color=(1, 1, 1),
                label=f"{label} IP{ip}",
                zorder=-1,
            )
            # txt = ax.text(
            #     x.value, y.value,
            #     s=str(ip),
            #     color=color,
            #     ha='center',
            #     va='center',
            #     fontsize=mpl.rcParams['lines.markersize'] * (1.2 if "w/ " not in label else 0.5),
            #     fontdict=dict(weight="bold"),
            #     )
            ax.add_patch(
                Ellipse(
                    xy=(x.value, y.value),
                    width=2*x.error,
                    height=2*y.error,
                    facecolor=pcolors.mc.to_rgba(color, 0.1),
                    # alpha=0.1,
                    edgecolor=pcolors.mc.to_rgba(color, 0.8),
                    linestyle="-",
                )
            )
            # ax.errorbar(
            #     x.value,
            #     y.value,
            #     xerr=x.error,
            #     yerr=y.error,
            #     marker=markers[ip],
            #     color=color,
            #     label=f"{label} IP{ip}",
            # )
        labels[idx] = label
        handles[idx] = Line2D([0], [0], marker=plines.text_to_marker(f"1,5"),
                              color=color,
                              markersize=25,
                              ls='')

    label = r"$K_6L$ {0}  $\left[10^3 \mathrm{{m}}^{{-5}}\right]$"
    ax.set_xlabel(label.format("Left"))
    ax.set_ylabel(label.format("Right"))

    # legend = pannot.make_top_legend(ax, ncol=2)
    # pmisc.square_axis_limits(ax)

    # IP1
    # ax.set_xlim([1.6, 1.9])
    # ax.set_ylim([-3.6, -3.3])
    # IP5
    # ax.set_xlim([4.3, 4.6])
    # ax.set_ylim([-3.45, -3.1])
    #
    ax.set_xlim([-1.5, 4.2])
    ax.set_ylim([-3.5, 0.2])
    # ax.set_xlim([-10, 10])
    # ax.set_ylim([-10, 10])
    ax.xaxis.set_major_locator(mticker.MultipleLocator(1))
    ax.xaxis.set_minor_locator(mticker.MultipleLocator(0.1))
    ax.yaxis.set_major_locator(mticker.MultipleLocator(1))
    ax.yaxis.set_minor_locator(mticker.MultipleLocator(0.1))

    pannot.make_top_legend(ax, ncol=2, handles=handles, labels=labels, transposed=True)
    plt.show()
    fig.savefig(OUTPUT / f"plot.wise_comparison_with_errors.pdf")
    # fig.savefig(OUTPUT / f"plot.wise_comparison_2021heads.pdf")

if __name__ == '__main__':
    pstyle.set_style(
        "standard",
        manual={
            "figure.figsize": [6.2, 4.5],
            "figure.subplot.left": 0.12,
            "figure.subplot.bottom": 0.15,
            "figure.subplot.right": 0.99,
            "figure.subplot.top": 0.77,
            "axes.grid": False,
            "markers.fillstyle": 'full',
            "lines.markersize": 11,
            # "ytick.minor.visible": True,
        }
    )
    # gather_wise_data()
    # gather_wise_data_jdilly()
    # plot_data_2011()
    # gather_wise_data_jdilly(heads=True)
    # plot_data_beambased_vs_2022_model()
    plot_data_beambased_vs_2022_model_with_errors()
# plot_data()

