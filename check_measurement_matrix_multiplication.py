#%% 
from utilities.classes import MeasureValue
import numpy as np
import pandas as pd



#%%
measures = pd.Series({"a": MeasureValue(2, 3), "b": MeasureValue(5, 4)})
#%%
meas_test = pd.Series(dtype=float)
meas_test = meas_test.append(measures)
meas_test


#%%
I = np.array([[1, 0], [0, 1]])
I.dot(measures)

# %%
A = np.array([[1, 1], [0, 1]])
A.dot(measures)
# %%
A = np.array([[1, 2, 3], [0, 1, 0], [-1, -1, -1]])
A.dot([MeasureValue(1, 2), MeasureValue(2, 3), MeasureValue(3, 4)])

