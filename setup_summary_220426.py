import shutil
from pathlib import Path

from correction_via_amplitude_detuning import main, log_setup
from classes import scaled_detuning, scaled_contraints, TargetData, Target, scaled_detuningmeasurement
from plotting import plot_detuning_ips, plot_detuning, plot_correctors

PLOT_STYLE = 'separate'

def ltx_dqd2j(tune, action, power=1):
    if power == 1:
        return f"$Q_{{{tune},{action}}}$"
    return f"$Q_{{{tune},{action}^{{{power}}}}}$"


def xing_ip5():
    xing = {'scheme': 'flat', 'on_x5': 160, 'on_sep5': 0.55, 'on_ov5': -1.8}
    targets = [
        Target(
            name="X10_ip5",
            data=TargetData(
                ips=5,
                detuning={
                    1: scaled_detuning(X10=-55.2),
                    2: scaled_detuning(X10=-9),
                },
            )
        ),
        Target(
            name="X10X01Y01_cross0_ip5",
            data=TargetData(
                ips=5,
                detuning={
                    1: scaled_detuning(X10=-55.2, X01=0, Y01=-6),
                    2: scaled_detuning(X10=-9, X01=0, Y01=-6),
                }
            )
        ),
        Target(
            name="X10X01Y01_cross50_ip5",
            data=TargetData(
                ips=5,
                detuning={
                    1: scaled_detuning(X10=-55.2, X01=-50, Y01=-6),
                    2: scaled_detuning(X10=-9, X01=-50, Y01=-6),
                }
            )
        ),
    ]
    paths = {i: Path(f"xing5_b1b4/b{i}") for i in (1, 4)}
    main(paths, xing=xing, targets=targets)


def plot_xing_ip5(fields="b6"):
    path = Path('xing5_b1b4')
    ids = [f"{s}_ip5_{fields}" for s in (
        'X10',
        'X10X01Y01_cross0',
        'X10X01Y01_cross50')]
    # labels=[f"{ltx_dqd2j('x', 'x')} = -55.2 | -9",
    #         f"{ltx_dqd2j('x', 'x')} = -55.2 | -9\n {ltx_dqd2j('x', 'y')} = 0\n {ltx_dqd2j('y', 'y')} = -6 | -6",
    #         f"{ltx_dqd2j('x', 'x')} = -55.2 | -9\n {ltx_dqd2j('x', 'y')} = -50\n {ltx_dqd2j('y', 'y')}  -6 | -6",
    #         ]
    labels = "ABC"
    output_id = f"_ip5_{fields}"
    plot_detuning(
        path,
        ids=ids,
        labels=labels,
        fields=fields,
        size=[3.4, 3.4],
        measurement={1: {"X10": [-55.2, 6], "Y01": [-6, 2.2]}, 4: {"X10": [-9, 0.7], "Y01": [-6, 1.4]}},
        beams=(1, 4),
        ylims={1: [-63, 63], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id,
        alternative=PLOT_STYLE,
    )

    corr_size = [6.00, 6.40]
    if 'b5' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr][15]', order="5", output_id=f'{output_id}.b5')

    if 'b6' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=f'{output_id}.b6')



# XING Full --------------------------------------------------------------------


def xing_full():
    xing = {'scheme': 'top'}
    targets = [
        # Global Targets ---
        Target(
            name="X10_constrX01_ip15",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: scaled_detuning(X10=-33.2),
                    2: scaled_detuning(X10=-4.5),
                },
                constraints={
                    1: scaled_contraints(X01="<=0"),
                    2: scaled_contraints(X01="<=0"),
                }
            ),
        ),
        Target(
            name="X10Y01X01_ip15",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: scaled_detuning(X10=-33.2, X01=0, Y01=35),
                    2: scaled_detuning(X10=-4.5, X01=0, Y01=-7),
                },
            ),
        ),
        Target(
            name="X10Y01_constrX01_ip15",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: scaled_detuning(X10=-33.2, Y01=35),
                    2: scaled_detuning(X10=-4.5, Y01=-7),
                },
                constraints={
                    1: scaled_contraints(X01="<=0"),
                    2: scaled_contraints(X01="<=0"),
                }
            ),
        ),
        Target(
            name="X10Y01_ip15",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: scaled_detuning(X10=-33.2, Y01=35),
                    2: scaled_detuning(X10=-4.5, Y01=-7),
                },
            ),
        ),
        # Local Targets ---
        Target(
            name="X10Y01_ip15_ip5",
            data=[
                TargetData(
                    ips=(1, 5,),
                    detuning={
                        1: scaled_detuning(X10=-33.2, Y01=35),
                        2: scaled_detuning(X10=-4.5, Y01=-7),
                    },
                ),
                TargetData(
                    ips=5,
                    detuning={
                        1: scaled_detuning(X10=-55.2, Y01=-6),
                        2: scaled_detuning(X10=-9, Y01=-6),
                    },
                ),

            ],
        ),
        Target(
            name="X10X01Y01_ip5_ip1",
            data=[
                TargetData(
                    ips=5,
                    detuning={
                        1: scaled_detuning(X10=-55.2, X01=0, Y01=-6),
                        2: scaled_detuning(X10=-9, X01=0, Y01=-6),
                    },
                ),
                TargetData(
                    ips=1,
                    detuning={
                        1: scaled_detuning(X10=22, X01=0, Y01=41),
                        2: scaled_detuning(X10=4.5, X01=0, Y01=-1),
                    },
                ),
            ]
        ),
        Target(
            name="X10Y01_ip5_ip1_constrX01",
            data=[
                TargetData(
                    ips=5,
                    detuning={
                        1: scaled_detuning(X10=-55.2, Y01=-6),
                        2: scaled_detuning(X10=-9, Y01=-6),
                    },
                    constraints={
                        1: scaled_contraints(X01="<=0"),
                        2: scaled_contraints(X01="<=0"),
                    }
                ),
                TargetData(
                    ips=1,
                    detuning={
                        1: scaled_detuning(X10=22, Y01=41),
                        2: scaled_detuning(X10=4.5, Y01=-1),
                    },
                    constraints={
                        1: scaled_contraints(X01="<=0"),
                        2: scaled_contraints(X01="<=0"),
                    }
                ),
            ]
        ),
        Target(
            name="X10Y01_ip5_ip1_ip15_constr20X01",
            data=[
                TargetData(
                    ips=5,
                    detuning={
                        1: scaled_detuning(X10=-55.2, Y01=-6),
                        2: scaled_detuning(X10=-9, Y01=-6),
                    },
                    constraints={
                        1: scaled_contraints(X01="<=20"),
                        2: scaled_contraints(X01="<=20"),
                    }
                ),
                TargetData(
                    ips=1,
                    detuning={
                        1: scaled_detuning(X10=22, Y01=41),
                        2: scaled_detuning(X10=4.5, Y01=-1),
                    },
                    constraints={
                        1: scaled_contraints(X01="<=20"),
                        2: scaled_contraints(X01="<=20"),
                    }
                ),
                TargetData(
                    ips=(1, 5,),
                    detuning={
                        1: scaled_detuning(X10=-33.2, Y01=35),
                        2: scaled_detuning(X10=-4.5, Y01=-7),
                    },
                    constraints={
                        1: scaled_contraints(X01="<=0"),
                        2: scaled_contraints(X01="<=0"),
                    }
                ),
            ]
        ),
        Target(
            name="X10Y01_ip5_ip1_ip15_constr30X01",
            data=[
                TargetData(
                    ips=5,
                    detuning={
                        1: scaled_detuning(X10=-55.2, Y01=-6),
                        2: scaled_detuning(X10=-9, Y01=-6),
                    },
                    constraints={
                        1: scaled_contraints(X01="<=30"),
                        2: scaled_contraints(X01="<=30"),
                    }
                ),
                TargetData(
                    ips=1,
                    detuning={
                        1: scaled_detuning(X10=22, Y01=41),
                        2: scaled_detuning(X10=4.5, Y01=-1),
                    },
                    constraints={
                        1: scaled_contraints(X01="<=30"),
                        2: scaled_contraints(X01="<=30"),
                    }
                ),
                TargetData(
                    ips=(1, 5,),
                    detuning={
                        1: scaled_detuning(X10=-33.2, Y01=35),
                        2: scaled_detuning(X10=-4.5, Y01=-7),
                    },
                    constraints={
                        1: scaled_contraints(X01="<=0"),
                        2: scaled_contraints(X01="<=0"),
                    }
                ),
            ]
        ),
        Target(
            name="X10X10Y01_ip5_ip1_constr0X01",
            data=[
                TargetData(
                    ips=5,
                    detuning={
                        1: scaled_detuning(X10=-55.2, X01=0, Y01=-6),
                        2: scaled_detuning(X10=-9, Y01=-6),
                    },
                    constraints={
                        2: scaled_contraints(X01="<=0"),
                    }
                ),
                TargetData(
                    ips=1,
                    detuning={
                        1: scaled_detuning(X10=22, X01=0, Y01=41),
                        2: scaled_detuning(X10=4.5, Y01=-1),
                    },
                    constraints={
                        2: scaled_contraints(X01="<=0"),
                    }
                ),
            ]
        ),
        Target(
            name="X10X10Y01_ip5_ip1_ip15_constr0X01",
            data=[
                TargetData(
                    ips=5,
                    detuning={
                        1: scaled_detuning(X10=-55.2, X01=0, Y01=-6),
                        2: scaled_detuning(X10=-9, Y01=-6),
                    },
                    constraints={
                        2: scaled_contraints(X01="<=0"),
                    }
                ),
                TargetData(
                    ips=1,
                    detuning={
                        1: scaled_detuning(X10=22, X01=0, Y01=41),
                        2: scaled_detuning(X10=4.5, Y01=-1),
                    },
                    constraints={
                        2: scaled_contraints(X01="<=0"),
                    }
                ),
                TargetData(
                    ips=(1, 5,),
                    detuning={
                        1: scaled_detuning(X10=-33.2, Y01=35),
                        2: scaled_detuning(X10=-4.5, Y01=-7),
                    },
                    constraints={
                        1: scaled_contraints(X01="<=0"),
                        2: scaled_contraints(X01="<=0"),
                    }
                ),
            ]
        ),
    ]

    paths = {i: Path(f"xing_b1b4/b{i}") for i in (1, 4)}
    main(paths, xing=xing, targets=targets)


def plot_xing_full(fields="b6"):
    path = Path('xing_b1b4')
    nchar = 10
    ids = [f"{s}_{fields}" for s in (
        'X10Y01_ip15',
        'X10Y01X01_ip15',
        'X10Y01_constrX01_ip15',
        'X10Y01_ip15_ip5',
        'X10X01Y01_ip5_ip1',
        "X10Y01_ip5_ip1_constrX01",
        "X10Y01_ip5_ip1_ip15_constr20X01",
        "X10Y01_ip5_ip1_ip15_constr30X01",
        # "X10X10Y01_ip5_ip1_constr0X01",
        # "X10X10Y01_ip5_ip1_ip15_constr0X01",
    )]
    labels = "DEFGHIJK"
    # labels = [
    #     f"{ltx_dqd2j('x', 'x')} = {'-33.2 | -4.5'.center(nchar)}\n{ltx_dqd2j('x', 'y')} <= {'0'.center(nchar)}",
    #     f"{ltx_dqd2j('x', 'x')} = {'55.2-22 | 9-4.5'.center(nchar)}",
    #     f"{ltx_dqd2j('x', 'x')} = {'55.2-22 | 9-4.5'.center(nchar)}\n{ltx_dqd2j('x', 'y')} = {'25+25'.center(nchar)}",
    #     f"{ltx_dqd2j('x', 'x')} = {'55.2-22 | 9-4.5'.center(nchar)}\n{ltx_dqd2j('x', 'y')} = {'0+0'.center(nchar)}\n${ltx_dqd2j('y', 'y')} =$ {'-35 | 7'.center(nchar)}",
    #     f"{ltx_dqd2j('x', 'x')} = {'55.2-22 | 9-4.5'.center(nchar)}\n{ltx_dqd2j('x', 'y')} = {'25+25'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | 6+1'.center(nchar)}",
    #     f"{ltx_dqd2j('x', 'x')} = {'55.2-22 | 9-4.5'.center(nchar)}\n{ltx_dqd2j('x', 'y')} = {'25+25'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | --'.center(nchar)}",
    #     f"{ltx_dqd2j('x', 'x')} = {'55.2-22 | 9-4.5'.center(nchar)}\n{ltx_dqd2j('x', 'y')} >= {'0+0'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | 6+1'.center(nchar)}",
    #     f"{ltx_dqd2j('x', 'x')} = {'55.2-22 | 9-4.5'.center(nchar)}\n{ltx_dqd2j('x', 'y')} >= {'0'.center(nchar)}\n${ltx_dqd2j('y', 'y')}$ = {'-41+6 | 6+1'.center(nchar)}",
    # ]

    output_id = f"_ip5_ip1_{fields}"
    plot_detuning_ips(
        path,
        ids=ids,
        labels=labels,
        fields=fields,
        size=[9., 4],
        measurement={1: scaled_detuningmeasurement(X10=(-33.2, 1.12), Y01=(35, 1.41)),
                     4: scaled_detuningmeasurement(X10=(-4.5, 1.12), Y01=(-7, 3.2))},
        beams=(1, 4),
        ylims={1: [-63, 63], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id,
        alternative=PLOT_STYLE,
    )
    corr_size = [6.00, 6.90]
    ncol = 4
    if 'b5' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kcdx3\.[lr][15]', order="5", output_id=f'{output_id}.b5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kcdx3\.[lr]5', order="5", output_id=f'{output_id}.b5_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kcdx3\.[lr]1', order="5", output_id=f'{output_id}.b5_ip1')

    if 'b6' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=f'{output_id}.b6')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}.b6_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}.b6_ip1')


# Measured Data only -----------------------------------------------------------

def xing_full_measured():
    xing = {'scheme': 'top'}
    targets = [
        Target(
            name="X10Y01_ip15",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: scaled_detuning(X10=-33.2, Y01=35),
                    2: scaled_detuning(X10=-4.5, Y01=-7),
                },
            ),
        ),
        Target(
            name="X10Y01_ip15_ip5",
            data=[
                TargetData(
                    ips=(1, 5,),
                    detuning={
                        1: scaled_detuning(X10=-33.2, Y01=35),
                        2: scaled_detuning(X10=-4.5, Y01=-7),
                    },
                ),
                TargetData(
                    ips=5,
                    detuning={
                        1: scaled_detuning(X10=-55.2, Y01=-6),
                        2: scaled_detuning(X10=-9, Y01=-6),
                    },
                ),
            ],
        ),
    ]
    paths = {i: Path(f"xing_measured_b1b4/b{i}") for i in (1, 4)}
    main(paths, xing=xing, targets=targets)


def plot_xing_measured(fields="b6"):
    path = Path('xing_measured_b1b4')
    nchar = 10
    ids = [f"{s}_{fields}" for s in (
        'X10Y01_ip15',
        'X10Y01_ip15_ip5',
    )]
    # labels = "AB"
    labels = [
        f"IP15:\n{ltx_dqd2j('x', 'x')} = {'-33.2 | -4.5'.center(nchar)}\n{ltx_dqd2j('y', 'y')} = {'35 | -7'.center(nchar)}",
        #
        f"IP15:\n{ltx_dqd2j('x', 'x')} = {'-33.2 | -4.5'.center(nchar)}\n{ltx_dqd2j('y', 'y')} = {'35 | -7'.center(nchar)}\n"
        f"IP5:\n{ltx_dqd2j('x', 'x')} = {'-55.2 | -9'.center(nchar)}\n{ltx_dqd2j('y', 'y')} = {'-6 | -6'.center(nchar)}\n",
        #
    ]

    output_id = f"_measured_{fields}"
    plot_detuning_ips(
        path,
        ids=ids,
        labels=labels,
        fields=fields,
        size=[7, 5.4],
        measurement={1: {"X10": [-33.2, 1.12], "Y01": [35, 1.41]}, 4: {"X10": [-4.5, 1.12], "Y01": [-7, 3.2]}},
        beams=(1, 4),
        ylims={1: [-63, 63], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id,
        alternative=PLOT_STYLE,
    )
    corr_size = [6.00, 6.90]
    ncol = 4
    if 'b5' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kcdx3\.[lr][15]', order="5", output_id=f'{output_id}.b5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kcdx3\.[lr]5', order="5", output_id=f'{output_id}.b5_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kcdx3\.[lr]1', order="5", output_id=f'{output_id}.b5_ip1')

    if 'b6' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=f'{output_id}.b6')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}.b6_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, ncol=ncol, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}.b6_ip1')


def copy_only_results(src: Path, dst: Path):
    for beam in ("b1", "b4"):
        src_dir = src / beam
        dst_dir = dst / beam
        for file in src_dir.glob("*"):
            if file.suffix in (".tfs", ".madx") and "nominal" not in file.name and "optics_ir" not in file.name:
                print(f"{str(file):<130s} ->  {str(dst_dir / file.name):s}")
                shutil.copy(file, dst_dir / file.name)

if __name__ == '__main__':
    log_setup()
    # xing_ip5()
    # plot_xing_ip5()
    # xing_full()
    plot_xing_full()
    # xing_full_measured()
    # plot_xing_measured()
    # copy_only_results(Path("/home/jdilly/Software/b6_correction_from_amplitude_detuning/xing_measured_b1b4"), Path("/home/jdilly/Software/b6_correction_from_amplitude_detuning/xing_b1b4"))
