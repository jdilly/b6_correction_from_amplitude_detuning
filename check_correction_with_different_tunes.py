"""
Quick check to see how much the corrections differ using injection and collision tunes.

To be run:
 - setup_MD6863_with_ip5_and_tunes
 - setup_MD6863_with_ip5_and_wrong_tunes
"""
import sys
from pathlib import Path

import pandas as pd

from utilities.lhc_simulation import get_detuning_from_ptc_output
import tfs
from setup_MD6863_with_ip5 import meas_full, meas_flat

import logging

LOGGER = logging.getLogger(__name__)


INJ_TUNES_PATH = Path("md6863_with_ip5_inj_tunes")
COL_TUNES_PATH = Path("md6863_with_ip5_col_tunes")


def to_str(s: pd.Series):
    return ", ".join(f"{k}: {v: 6.2f}" for k, v in s.to_dict().items())


def log_values(correction, nominal, target):
    diff = correction - nominal
    diff_to_target = diff - target

    LOGGER.info(f"Corrected : {to_str(diff*1e-3)}")
    LOGGER.info(f"Target    : {to_str(target*1e-3)}")
    LOGGER.info(f"DiffTarget: {to_str(diff_to_target*1e-3)}")
    LOGGER.info(f"%         : {to_str(diff_to_target / target * 100)}")


def main():
    
    for beam in (1, 4):

        ampdet_inj_nominal = pd.Series(get_detuning_from_ptc_output(tfs.read(INJ_TUNES_PATH / f"full_b{beam}" / f"ampdet.lhc.b{beam}.nominal.tfs")))
        ampdet_col_nominal = pd.Series(get_detuning_from_ptc_output(tfs.read(COL_TUNES_PATH / f"full_b{beam}" / f"ampdet.lhc.b{beam}.nominal.tfs")))

        ampdet_inj_corr_with_inj = pd.Series(get_detuning_from_ptc_output(tfs.read(INJ_TUNES_PATH / f"b{beam}" / f"ampdet.lhc.b{beam}.X10X01Y01_full_and_ip5pm_b6_inj_tunes.tfs")))
        ampdet_inj_corr_with_col = pd.Series(get_detuning_from_ptc_output(tfs.read(COL_TUNES_PATH / f"b{beam}" / f"ampdet.lhc.b{beam}.X10X01Y01_full_and_ip5pm_b6_inj_tunes.tfs")))

        ampdet_col_corr_with_inj = pd.Series(get_detuning_from_ptc_output(tfs.read(INJ_TUNES_PATH / f"b{beam}" / f"ampdet.lhc.b{beam}.X10X01Y01_full_and_ip5pm_b6_col_tunes.tfs")))
        ampdet_col_corr_with_col = pd.Series(get_detuning_from_ptc_output(tfs.read(COL_TUNES_PATH / f"b{beam}" / f"ampdet.lhc.b{beam}.X10X01Y01_full_and_ip5pm_b6_col_tunes.tfs")))

        target_detuning = (meas_flat[beam if beam == 1 else 2]- meas_full[beam if beam == 1 else 2]).get_detuning()
        target = pd.Series({t: getattr(target_detuning, t) for t in ["X10", "Y01", "X01"]})

        LOGGER.info(f"Beam {beam}")
        LOGGER.info(f"Collision tunes Nominal:")
        LOGGER.info(f"Val : {to_str(ampdet_col_nominal)}")
        LOGGER.info(f"")
        LOGGER.info(f"Collision tunes w/ collision tunes correction:")
        log_values(ampdet_col_corr_with_col, ampdet_col_nominal, target)
        LOGGER.info(f"")
        LOGGER.info(f"Collision tunes w/ injection tunes correction:")
        log_values(ampdet_col_corr_with_inj, ampdet_col_nominal, target)
        LOGGER.info(f"")
        LOGGER.info(f"Injection tunes w/ injection tunes correction:")
        log_values(ampdet_inj_corr_with_inj, ampdet_inj_nominal, target)
        LOGGER.info(f"")
        LOGGER.info(f"Injection tunes w/ collision tunes correction:")
        log_values(ampdet_inj_corr_with_col, ampdet_inj_nominal, target)
        LOGGER.info(f"")
        LOGGER.info(f"")




if __name__ == '__main__':
    logging.basicConfig(
        stream=sys.stdout,
        level=logging.INFO,
        format="%(message)s"
    )
    main()
