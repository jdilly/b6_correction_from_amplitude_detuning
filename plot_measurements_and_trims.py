"""
Plot the simulated detuning changes from the different
correction trims between measuring dates.
"""
from pathlib import Path
from typing import Dict

import pandas as pd
from matplotlib import pyplot as plt
from matplotlib.lines import Line2D

import tfs
from omc3.plotting.utils import annotations as pannot, colors as pcolors
from omc3.utils import logging_tools
from plot_measurements_comparison import data
from utilities.plot_utils_measurements_comparison import plot_measurements, MeasurementSetup, get_ylabel
from utilities.utils import log_setup

LOG = logging_tools.get_logger(__name__)

commish_flat_orbit = "2022-05-09T00:20:00"
commish_flat_orbit_b6 = "2022-06-04T16:34:00"
md6863_flat_orbit = "2022-06-24T23:01:00"

DATES_MAP = {
    "flat":
        {
            commish_flat_orbit: "commish2022_nob6_flat",
            commish_flat_orbit_b6: "commish2022_wb6_flat",
            md6863_flat_orbit: "md6863_nob6_flat",
        },
    "xing":
        {
            commish_flat_orbit: "commish2022_nob6_xing",
            commish_flat_orbit_b6: "commish2022_wb6_xing",
            md6863_flat_orbit: "md6863_nob6_xing",
        },
}

DATA_PATH = Path(__file__).parent / 'differences_trims'
SAVE_FIGS = True


def gather(crossing):
    for beam in (1, 2):
        df_gather = pd.DataFrame(index=["X10", "Y01", "X01"], columns=DATES_MAP.keys())
        for time in DATES_MAP[crossing].keys():
            input_path = DATA_PATH / crossing / f"output_{time}" / f"b{beam}" / f"ampdet.lhc.b{beam}.trims.tfs"
            df = tfs.read_tfs(input_path, index="NAME")
            detuning = get_detuning_from_ptc_output(df, beam)
            for term, value in detuning.items():
                df_gather.loc[term, time] = value
        tfs.write_tfs(DATA_PATH / crossing / f"gathered.b{beam}.tfs", df_gather, save_index="TERM")


def get_detuning_from_ptc_output(df: pd.DataFrame, beam: int = None, log: bool = True) -> Dict[str, float]:
    """ Convert PTC amplitude detuning output to dict and log values.

    Args:
        df (DataFrame): DataFrame as given by PTC.
        beam (int): Beam used (for logging purposes only)
        log (bool): Print values to the logger

    Returns:
        dict[str, float]: Dictionary with entries 'X', 'Y', 'XY'
        with the values for the direct X, direct Y and cross Term respectively

    """
    names = ["X10", "Y01", "X01"]
    results = {name: None for name in names}
    if log:
        LOG.info("Current Detuning Values" + ("" if not beam else f" in Beam {beam}"))
    for term in names:
        value = df.query(
            f'NAME == "ANH{term[0]}" and '
            f'ORDER1 == {term[1]} and ORDER2 == {term[2]} '
            f'and ORDER3 == 0 and ORDER4 == 0'
        )["VALUE"].to_numpy()[0]
        if log:
            LOG.info(f"  {term:<3s}: {value}")
        results[term] = value
    return results



def plot_between_times(manual_style, crossing):
    """
    To Plot:
      - (Xing - Flat) before b6
      - Expected after correction: (Xing + Correction) - Flat
      - (Xing - Flat) after b6
    """
    out_dir = DATA_PATH / crossing
    dates_map = DATES_MAP[crossing]
    for beam in (1, 2):
        date_first = data[dates_map[commish_flat_orbit_b6]][beam] - data[dates_map[commish_flat_orbit]][beam]
        date_second = data[dates_map[md6863_flat_orbit]][beam] - data[dates_map[commish_flat_orbit_b6]][beam]
        meas = [
            MeasurementSetup(measurement=date_first, label="Between 09.05 and 04.06"),
            MeasurementSetup(measurement=date_second, label="Between 04.06 and 24.06"),
        ]
        fig = plot_measurements(meas,
                                manual_style=manual_style,
                                rescale=3,
                                ylabel=get_ylabel(rescale=3, delta=True),
                                ylim=[-30, 30] if crossing == "flat" else [-70, 70],
                                add_rms=False,
                                ncol=1,
                                )

        gathered = tfs.read(DATA_PATH / crossing / f"gathered.b{beam}.tfs", index="TERM")
        sim_data = [
            gathered[commish_flat_orbit_b6] - gathered[commish_flat_orbit],
            gathered[md6863_flat_orbit] - gathered[commish_flat_orbit_b6],
        ]
        measurement_width = 1 / (len(meas) + 1)
        scale = 1e-3
        ax = fig.gca()

        for idx_term, term in enumerate(gathered.index):
            for idx_id, data_id in enumerate(sim_data):
                x_pos = idx_term + (idx_id+1) * measurement_width
                y_pos = data_id.loc[term]*scale
                ax.plot(x_pos, y_pos, color=pcolors.get_mpl_color(idx_id), marker="o", ls="none", label=f"_{beam}.{term}")

        handles, labels = ax.get_legend_handles_labels()
        handles.append(Line2D([0], [0], marker="o", color="grey", ls="none"))
        labels.append("Simulation")
        pannot.make_top_legend(ax, handles=handles, labels=labels, ncol=1, frame=False)

        if SAVE_FIGS:
            fig.savefig(out_dir / f"plot.detuning_change_trims.b{beam:d}.pdf")


if __name__ == '__main__':
    log_setup()
    # print_correction_expectation()

    manual_style = {
        # "font.size": 22.5,
        # "figure.constrained_layout.use": False,
        "figure.figsize": [4.50, 3.0],
        "figure.subplot.left": 0.12,
        "figure.subplot.bottom": 0.15,
        "figure.subplot.right": 0.99,
        "figure.subplot.top": 0.77,
        "errorbar.capsize": 5,
        "lines.marker": "x",
        "lines.markersize": 4,
        "axes.grid": False,
        "ytick.minor.visible": True,
    }
    # gather(crossing="xing")
    plot_between_times(manual_style, crossing="flat")
    plot_between_times(manual_style, crossing="xing")
    plt.show()