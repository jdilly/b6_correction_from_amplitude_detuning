from detuning_calculation import calculate_values_feeddown_to_detuning, get_knl_to_circuit_map_for_correctors


def calculate_detuning(optics: AllOptics, target: Target) -> Dict[str, float]:
    """ Calculates the values for either kcdx or kctx as installed into the Dodecapol corrector.
    Returns a dictionary of circuit names and their settings in KNL values (i.e. needs to be divided by the lenght of the decapole corrector).
    """
    circuits_map = get_knl_to_circuit_map_for_correctors(target.ips)

    m = pd.DataFrame(columns=list(circuits_map.keys()))
    v = pd.Series(dtype=float)
    m_constr = pd.DataFrame(columns=list(circuits_map.keys()))
    v_constr = pd.Series(dtype=float)

    for target_data in target.data:
        try:
            use_optics = optics[target_data.xing]
        except KeyError:
            use_optics = optics

        res_tdata = build_full_feeddown_matrix(
            optics=use_optics, detuning_data=target_data,
            magnet_pattern=DODECAPOLE_CORRECTOR
        )
        m = m.append(res_tdata[0])
        v = v.append(res_tdata[1])
        m_constr = m_constr.append(res_tdata[2])
        v_constr = v_constr.append(res_tdata[3])

    # Select only the used circuits (given by the fields) and filter equation system
    columns_mask = (m.columns.str.endswith("b5") & ("5" in fields)) | (m.columns.str.endswith("b6") & ("6" in fields))
    m = m.loc[:, columns_mask].rename(columns=circuits_map).fillna(0.)
    m_constr = m_constr.loc[:, columns_mask].rename(columns=circuits_map).fillna(0.)

    # Solve as convex system
    x = cp.Variable(len(m.columns))
    cost = cp.sum_squares(m.to_numpy() @ x - v)  # ||Mx - v||_2
    if len(v_constr):
        # Add constraints
        constr = m_constr.to_numpy() @ x <= v_constr.to_numpy()
        prob = cp.Problem(cp.Minimize(cost), [constr])
    else:
        # No constraints
        prob = cp.Problem(cp.Minimize(cost))
    prob.solve()
    if prob.status in ["infeasible", "unbounded"]:
        raise ValueError(f"Optimization failed! Reason: {prob.status}.")

    return {
        name: value for name, value in zip(m.columns, x.value)
    }

def main(outputdirs: Dict[int, Path],
         targets: Sequence[Target],
         field_list: Sequence[str] =('b6',),
         xing: dict = None,  # set to {'scheme': 'top'} below
         optics: str = 'round3030',  # 30cm round optics
         year: int = 2018,  # lhc year
         ):
    """ Main function to run this script."""
    # set mutable defaults ----
    if xing is None:
        xing = {'scheme': 'top'}  # use top-energy crossing scheme

    target_names = [t.name for t in targets]
    if len(target_names) != len(set(target_names)):
        raise KeyError("Some targets have the same names. "
                       "This will lead to outputfiles for these targets to be overwritten. "
                       "Please give them unique names.")

    # Setup LHC for both beams -------------------------------------------------
    lhc_beams: Dict[int, LHCBeam] = {}
    for beam, outputdir in outputdirs.items():
        lhc_beam = LHCBeam(
            beam=beam, outputdir=outputdir,
            xing=xing, optics=optics, year=year,
        )
        lhc_beams[beam] = lhc_beam
        lhc_beam.setup_machine()
        lhc_beam.save_nominal(id_=None)

    # Compensate amplitude detuning --------------------------------------------
    optics = {}
    for beam, lhc_beam in lhc_beams.items():
        lhc_beam.install_circuits_into_mctx()
        optics[beam] = lhc_beam.df_twiss_nominal_ir.copy()

        for target in targets:
            LOG.info(f"Calculating detuning with for \n{str(target)}")
            try:
                values = calculate_values_feeddown_to_detuning(optics, target=target, fields=fields)
            except ValueError:
                LOG.error(f"Optimization failed for {target.name} and {fields}.")
                values = {}
            dfs_effective_detuning = calc_effective_detuning(optics, values, ips=target.ips)

            for lhc_beam, df in zip(lhc_beams.values(), dfs_effective_detuning):
                tfs.write(lhc_beam.output_path('ampdet_calc', id_), df)
                lhc_beam.set_mctx_circuits_powering(values, id_=id_)
                lhc_beam.check_kctx_limits()
                lhc_beam.reset_detuning_circuits()

    # exit
    for lhc_beam in lhc_beams.values():
        lhc_beam.madx.exit()
