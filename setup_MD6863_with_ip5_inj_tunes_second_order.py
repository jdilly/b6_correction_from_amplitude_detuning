"""
MD6863 - Detuning as measured during MD6863 (24.06.2022)
--------------------------------------------------------

This data is used to calculated b6 corrections.
"""
from pathlib import Path
from typing import Dict, Any, Sequence

import tfs
from correction_via_amplitude_detuning_multi_xing import (
    create_optics, calculate_corrections, check_corrections
)
from tfs.tools import significant_digits
from utilities.utils import log_setup
from utilities.classes import (
    scaled_detuning, scaled_contraints, scaled_detuningmeasurement,
    TargetData, Target,
    DetuningMeasurement, Detuning, MeasureValue,
)
from utilities.plotting import plot_detuning_ips, plot_correctors
import check_second_order_influence_on_fit as csoiof


def get_sum(meas_a, meas_b):
    return {beam: meas_a[beam] + meas_b[beam] for beam in meas_a.keys()}


def get_diff(meas_a, meas_b):
    return {beam: meas_a[beam] - meas_b[beam] for beam in meas_a.keys()}

# Define Machine Data
# -------------------

PREFIX = ""

# Output Path
output = Path(f"md6863_with_ip5_inj_tunes_{PREFIX}new_fit")

# Fill in special values for crossing if needed:
xings = {
    'full': {'scheme': 'flat', 'on_x1': -160, 'on_x5': 160},
    'ip5+': {'scheme': 'flat', 'on_x5': 160},
    'ip5-': {'scheme': 'flat', 'on_x5': -160},
}


# Fill in measurement data in 10^3 m^-1:
def get_measured_detuning_values(prefix: str = PREFIX):
    """ Get the detuning values from summary file (see check_second_order_influence_on_fit.py). """
    labels = {
        "1_OFF_5_OFF_b6_out":  "meas_flat",
        "1_-160_5_+160_b6_out": "meas_full",
        "1_Off_5_+160_b6_out":  "meas_ip5p",
        "1_Off_5_-160_b6_out":  "meas_ip5m",
    }
    data = {}
    df = tfs.read(csoiof.OUTPUT_DATA / "summary_merged.tfs", index="Result")
    for meas, label in labels.items():
        data[label] = {}
        for idx, beam in enumerate((1, 2)):
            beam_label = f"B{beam}_{meas}"
            beam_data = {}
            plane2nums = {"X": "10", "Y": "01"}

            for tune, action in (("X", "X"), ("Y", "Y"), ("Y", "X"), ("X", "Y")):
                acd_scale = 0.5 if (tune == action) else 1
                value_col, error_col = f"{prefix}{csoiof.column_name(tune, action)}", f"{prefix}{csoiof.err_column_name(tune, action)}"
                value, error = df.loc[beam_label, value_col], df.loc[beam_label, error_col]
                beam_data[f"{tune}{plane2nums[action]}"] = MeasureValue(value * acd_scale, error * acd_scale)
                # value_str, error_str = significant_digits(value * scale * acd_scale, error * scale * acd_scale)
            beam_data["X01"] = (beam_data.pop("X01") + beam_data.pop("Y10")) * 0.5
            data[label][beam] = DetuningMeasurement(**beam_data)
    return data


# Steps of calculations --------------------------------------------------------

def ltx_dqd2j(tune, action, power=1):
    if power == 1:
        return f"$Q_{{{tune},{action}}}$"
    return f"$Q_{{{tune},{action}^{{{power}}}}}$"


def get_detuning(meas: Dict[Any, DetuningMeasurement]) -> Dict[Any, Detuning]:
    return {beam: meas[beam].get_detuning() for beam in meas.keys()}


def get_targets() -> Sequence[Target]:
    data = get_measured_detuning_values()
    detuning_flat = data["meas_flat"]
    detuning_full = data["meas_full"]
    detuning_ip5p = data["meas_ip5p"]
    detuning_ip5m = data["meas_ip5m"]
    targets = [
        Target(
            name="X10X01Y01_full_and_ip5pm",
            data=[
                TargetData(
                    ips=(1, 5),
                    detuning=get_diff(detuning_flat, detuning_full),
                    xing='full',
                ),
                TargetData(
                    ips=(5,),
                    detuning=get_diff(detuning_flat, detuning_ip5p),
                    xing='ip5+',
                ),
                TargetData(
                    ips=(5,),
                    detuning=get_diff(detuning_flat, detuning_ip5m),
                    xing='ip5-',
                ),
            ]
        ),
        # # TARGET TEMPLATE
        # Target(
        #     name="X10X01Y01_",
        #     data=[
        #         TargetData(
        #             ips=(1, ),
        #             detuning={
        #                 1: scaled_detuning(X10=0, X01=0, Y01=0),
        #                 2: scaled_detuning(X10=0, X01=0, Y01=0),
        #             },
        #             constraints={
        #                 1: scaled_constraints(X01="<=0"),
        #                 2: scaled_constraints(X01="<=0"),
        #             }
        #         ),
        #     ]
        # )
    ]
    # print(targets)
    # exit()
    return targets


def simulation():
    paths = {i: output / f"b{i}" for i in (1, 4)}
    lhc_beams = create_optics(paths, xings=xings, year=2022, tune_x=62.28, tune_y=60.31)
    for beams in lhc_beams.values():
        for lhcbeam in beams.values():
            lhcbeam.madx.exit()

    calculate_corrections(paths, targets=get_targets(), main_xing='full')
    check_corrections(paths, xing=xings['full'], tune_x=62.28, tune_y=60.31, year=2022, id_suffix="_inj_tunes")


def do_correction_only():
    paths = {i: output / f"b{i}" for i in (1, 4)}
    calculate_corrections(paths, targets=get_targets(), main_xing='full')


def plotting():
    data = get_measured_detuning_values()
    action_map = {"10": 'x', "01": 'y'}

    nchar = 10
    measurement = get_diff(data["meas_flat"], data["meas_full"])
    measurement[4] = measurement.pop(2)

    targets = get_targets()
    ids = [f"{target.name}_b6" for target in targets]
    terms = ("X10", "X01", "Y01")
    scaled = [{term: targets[0].data[0][b][term]*1e-3 for term in terms} for b in (1, 2)]
    labels = [
        "\n".join([
            f"{ltx_dqd2j(term[0], action_map[term[1:]])} = {f'{scaled[0][term]:.1f} | {scaled[1][term]}'.center(nchar)}"
            for term in terms
            ]
        )
    ]

    output_id = f"_corrections"
    plot_detuning_ips(
        output,
        ids=ids,
        labels=labels,
        fields="b6",
        size=[6., 3.9],
        measurement=measurement,
        beams=(1, 4),
        ylims={1: [-62, 62], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id,
        alternative="separate",  # "separate", "normal"
        delta=True,
    )
    corr_size = [4.80, 4.00]

    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=f'{output_id}_b6')
    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}_b6_ip5')
    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}_b6_ip1')


if __name__ == '__main__':
    log_setup()
    # get_targets()
    simulation()
    # do_correction_only()
    plotting()
