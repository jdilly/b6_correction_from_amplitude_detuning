import itertools
from pathlib import Path

import tfs

from correction_via_amplitude_detuning import fakeLHCBeam
from utilities.detuning_calculation import DODECAPOLE_CORRECTOR, get_detuning_coeff
from utilities.lhc_simulation import LHCBeam

DETUNING_TERMS = ("X10", "X01", "Y01")


def create_itertable(misalignment, bbeat) -> tfs.TfsDataFrame:
    params = ("BETX", "BETY", "X", "Y")

    d = {}
    for ip in (1, 5):
        for side in "LR":
            for param in params:
                name = f"IP{ip}{side}{param}"
                if param in "XY":
                    if misalignment:
                        values = [-misalignment, misalignment]
                    else:
                        values = [0.]
                else:
                    if bbeat:
                        values = [-bbeat, bbeat]
                    else:
                        values = [0.]

                d[name] = values

    df = tfs.TfsDataFrame(data=itertools.product(*d.values()), columns=list(d.keys()))
    return df



def main(input_dir, correction_name, misalignment, bbeat):
    inputdirs = {b: input_dir / f"b{b}" for b in (1, 4)}

    lhc_beams_in = {b: fakeLHCBeam(beam=b, outputdir=indir) for b, indir in inputdirs.items()}
    optics = {lhc_in.beam: tfs.read(LHCBeam.output_path(lhc_in, 'twiss', correction_name), index="NAME") for lhc_in in lhc_beams_in.values()}
    q = {b: tfs.TfsDataFrame(0.,
                             columns=DETUNING_TERMS,
                             index=["value", "min", "max", "mean", "std", "absmin"],
                             headers={"Beam": b, "BetaBeat": bbeat, "Misalignment": misalignment})
         for b in inputdirs.keys()}

    for beam, twiss in optics.items():
        for term in DETUNING_TERMS:
            for ip in (1, 5):
                for side in "LR":
                    magnet = DODECAPOLE_CORRECTOR.format(side=side, ip=ip)
                    beta = {p: twiss.loc[magnet, f"BET{p}"] for p in "XY"}
                    x, y = twiss.loc[magnet, "X"], twiss.loc[magnet, "Y"]
                    coeff = get_detuning_coeff(term, beta)
                    k6l = twiss.loc[magnet, "K5L"]
                    q[beam].loc["value", term] += 0.5 * (x**2 - y**2) * coeff * k6l

    iterable_df = create_itertable(misalignment=misalignment, bbeat=bbeat)
    q_iter = {b: tfs.TfsDataFrame(0., columns=DETUNING_TERMS, index=iterable_df.index, headers={"Beam": b}) for b in inputdirs.keys()}
    for beam, twiss in optics.items():
        for idx, values in iterable_df.iterrows():
            for term in DETUNING_TERMS:
                for ip in (1, 5):
                    for side in "LR":
                        name = f"IP{ip}{side}"
                        magnet = DODECAPOLE_CORRECTOR.format(side=side, ip=ip)
                        beta = {p: twiss.loc[magnet, f"BET{p}"] * (1+values[f"{name}BET{p}"]) for p in "XY"}
                        x, y = twiss.loc[magnet, "X"]+values[f"{name}X"], twiss.loc[magnet, "Y"]+values[f"{name}Y"]
                        coeff = get_detuning_coeff(term, beta)
                        k6l = twiss.loc[magnet, "K5L"]
                        q_iter[beam].loc[idx, term] += 0.5 * (x ** 2 - y ** 2) * coeff * k6l

    for beam in q.keys():
        q[beam].loc["min", :] = q_iter[beam].min()
        q[beam].loc["absmin", :] = q_iter[beam].abs().min()
        q[beam].loc["max", :] = q_iter[beam].max()
        q[beam].loc["mean", :] = q_iter[beam].mean()
        q[beam].loc["std", :] = q_iter[beam].std()

    # circuits_map = get_knl_to_circuit_map_for_correctors((1,5))
    # optics_nom = {lhc_in.beam: tfs.read(LHCBeam.output_path(lhc_in, 'twiss', 'optics_ir'), index="NAME") for lhc_in in lhc_beams_in.values()}
    # knl_map = {v: k for k, v in circuits_map.items()}
    # values = {circuits_map[f"{magnet}_b6"]: optics[1].loc[magnet, "K5L"] for magnet in optics[1].index}
    # dfs_effective_detuning = calc_effective_detuning(optics, values, ips=(1, 5))
    # b1 = dfs_effective_detuning[0].query("FIELDS == 'b6' & IP == 'all'")[list(DETUNING_TERMS)].iloc[0].to_dict()
    # b4 = dfs_effective_detuning[1].query("FIELDS == 'b6' & IP == 'all'")[list(DETUNING_TERMS)].iloc[0].to_dict()
    # print(b1)
    # print(b4)
    #
    # dfs_effective_detuning = calc_effective_detuning(optics_nom, values, ips=(1, 5))
    # b1 = dfs_effective_detuning[0].query("FIELDS == 'b6' & IP == 'all'")[list(DETUNING_TERMS)].iloc[0].to_dict()
    # b4 = dfs_effective_detuning[1].query("FIELDS == 'b6' & IP == 'all'")[list(DETUNING_TERMS)].iloc[0].to_dict()
    # print(b1)
    # print(b4)
    return q


if __name__ == '__main__':
    misalignment = 0.0
    bbeat = 0.1
    res = main(Path("md6863_with_ip5"), "X10X01Y01_full_and_ip5pm_b6", misalignment=misalignment, bbeat=bbeat)
    for beam in res.keys():
        tfs.write(f"stats_{misalignment*1e3:0.2f}mm_{bbeat:0.2f}beat_b{beam}.tfs", res[beam], save_index="Statistic")

    # create_itertable()