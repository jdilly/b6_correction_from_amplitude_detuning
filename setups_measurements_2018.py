from pathlib import Path

from correction_via_amplitude_detuning import main, calculate_from_prerun_optics
from classes import Detuning, Constraints, TargetData, Target
from plotting import plot_detuning_folders, ltx_dqd2j, plot_detuning, plot_correctors, plot_detuning_ips


def ip5():
    xing = {'scheme': 'flat', 'on_x5': 160, 'on_sep5': 0.55, 'on_ov5': -1.8}
    targets = [
        Target(
            name="X10_ip5",
            data=TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2),
                    2: Detuning(X10=9),
                },
            )
        ),
        Target(
            name="X10X01_cross0_ip5",
            data=TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=0),
                    2: Detuning(X10=9, X01=0),
                },
            )
        ),
        Target(
            name="X10X01_cross20_ip5",
            data=TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=20),
                    2: Detuning(X10=9, X01=20),
                },
            )
        ),
        Target(
            name="X10X01_cross50_ip5",
            data=TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=50),
                    2: Detuning(X10=9, X01=50),
                },
            )
        ),
        Target(
            name="X10X01Y01_ip5",
            data=TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=0, Y01=6),
                    2: Detuning(X10=9, X01=0, Y01=6),
                }
            )
        ),
    ]
    paths = {i: Path(f"nominal_xing5_b1b4/b{i}") for i in (1, 4)}
    main(paths, xing=xing, targets=targets)


def ip5_ip1():
    xing = {'scheme': 'top'}
    targets = [
        Target(
            name="X10_ip15",
            data=TargetData(
                ips=(1, 5),
                detuning={
                    1: Detuning(X10=33.2),
                    2: Detuning(X10=4.5),
                },
            )
        ),
        Target(
            name="X10_ip5_ip1",
            data=[TargetData(
                ips=(5,),
                detuning={
                    1: Detuning(X10=55.2),
                    2: Detuning(X10=9),
                }),
                TargetData(
                    ips=(1,),
                    detuning={
                        1: Detuning(X10=-22),
                        2: Detuning(X10=-4.5),
                    }),
            ],
        ),
        Target(
            name="X10X01_cross0_ip15",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: Detuning(X10=33.2, X01=0),
                    2: Detuning(X10=4.5, X01=0),
                }),
        ),
        Target(
            name="X10X01_cross0_ip5_ip1",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=0),
                    2: Detuning(X10=9, X01=0),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=0),
                        2: Detuning(X10=-4.5, X01=0),
                    }),
            ],
        ),
        Target(
            name="X10X01_cross20_ip15",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: Detuning(X10=33.2, X01=20),
                    2: Detuning(X10=4.5, X01=20),
                }),
        ),
        Target(
            name="X10X01_cross1010_ip5_ip1",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=10),
                    2: Detuning(X10=9, X01=10),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=10),
                        2: Detuning(X10=-4.5, X01=10),
                    }),
            ],
        ),
        Target(
            name="X10X01_cross50_ip15",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: Detuning(X10=33.2, X01=50),
                    2: Detuning(X10=4.5, X01=50),
                }),
        ),
        Target(
            name="X10X01_cross2525_ip5_ip1",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=25),
                    2: Detuning(X10=9, X01=25),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=25),
                        2: Detuning(X10=-4.5, X01=25),
                    }),
            ],
        ),
        Target(
            name="X10X01_cross5000_ip5_ip1",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=50),
                    2: Detuning(X10=9, X01=50),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=0),
                        2: Detuning(X10=-4.5, X01=0),
                    }),
            ],
        ),
        Target(
            name="X10X01_cross0050_ip5_ip1",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=0),
                    2: Detuning(X10=9, X01=0),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=50),
                        2: Detuning(X10=-4.5, X01=50),
                    }),
            ],
        ),
        Target(
            name="X10X01Y01_ip15",
            data=TargetData(
                ips=(1, 5,),
                detuning={
                    1: Detuning(X10=33.2, X01=0, Y01=-35),
                    2: Detuning(X10=4.5, X01=0, Y01=7),
                }),
        ),
        Target(
            name="X10X01Y01_ip5_ip1",
            data=[TargetData(
                ips=5,
                detuning={
                    1: Detuning(X10=55.2, X01=0, Y01=6),
                    2: Detuning(X10=9, X01=0, Y01=6),
                }),
                TargetData(
                    ips=1,
                    detuning={
                        1: Detuning(X10=-22, X01=0, Y01=-41),
                        2: Detuning(X10=-4.5, X01=0, Y01=1),
                    }),
            ]
        ),
    ]
    paths = {i: Path(f"nominal_xing_b1b4/b{i}") for i in (1, 4)}
    main(paths, xing=xing, targets=targets)
    # calculate_from_prerun_optics(inputdirs=paths, outputdirs={i: Path(f"nominal_xing_b1b4_calc/b{i}") for i in (1, 4)}, targets=targets)


def plot_X10X01Y01():
    meas_ip5_cross = {1: {"X10": [55.2, 6], "Y01": [6, 2.2]}, 4: {"X10": [9, 0.7], "Y01": [6, 1.4]}, 2: {"X10": [9, 0.7], "Y01": [6, 1.4]}}
    plot_detuning_folders(
        folders=[Path("nominal_ip5_X10_b1b4"), Path("nominal_ip5_X10X01_b1b4"), Path("nominal_ip5_X10X01_pos20_b1b4"), Path("nominal_ip5_X10X01Y01_b1b4")],
        # labels=["X10", "X10 & X01=0", "X10 & X01=50"],
        labels=["$\partial Q_x / \partial (2J_x) $ = meas",
                "$\partial Q_x / \partial (2J_x)$ = meas\n $\partial Q_x / \partial (2J_y) = 0\; \mathrm{m}^{-1}$",
                "$\partial Q_x / \partial (2J_x)$ = meas\n $\partial Q_x / \partial (2J_y) = 20 \cdot 10^3 \; \mathrm{m}^{-1}$",
                "$\partial Q_x / \partial (2J_x)$ = meas\n $\partial Q_x / \partial (2J_y) = 0 \; \mathrm{m}^{-1}$\n $\partial Q_y / \partial (2J_y) = 6 \; \mathrm{m}^{-1}$",
                ],
        tickrotation=0,
        size=[9.60, 4.80],
        id_="w_ampdet_b6", output_id="ip5_b6_correction", measurement=meas_ip5_cross, beams=(1, 4), ylims={1: [-62, 62], 2:[-3, 3]})


def plot_xing_ip5(fields):
    path = Path('nominal_xing5_b1b4')
    ids = [f"w_ampdet_{s}_ip5_{fields}" for s in ('X10', 'X10X01_cross0', 'X10X01_cross20', 'X10X01_cross50', 'X10X01Y01')]
    labels=[f"${ltx_dqd2j('x', 'x')}$ = 55.2 | 9",
            f"${ltx_dqd2j('x', 'x')}$ = 55.2 | 9\n ${ltx_dqd2j('x', 'y')} = 0 $",
            f"${ltx_dqd2j('x', 'x')}$ = 55.2 | 9\n ${ltx_dqd2j('x', 'y')} = 20 $",
            f"${ltx_dqd2j('x', 'x')}$ = 55.2 | 9\n ${ltx_dqd2j('x', 'y')} = 50 $",
            f"${ltx_dqd2j('x', 'x')}$ = 55.2 | 9\n ${ltx_dqd2j('x', 'y')} = 0 $\n ${ltx_dqd2j('y', 'y')} = 6 $",
            ]
    plot_detuning(
        path,
        ids=ids,
        labels=labels,
        size=[10.24, 4.80],
        measurement={1: {"X10": [55.2, 6], "Y01": [6, 2.2]}, 4: {"X10": [9, 0.7], "Y01": [6, 1.4]}},
        beams=(1, 4),
        ylims={1: [-62, 62], 2: [-3, 3]},
        tickrotation=0,
        output_id=f"_{fields}_correction"
    )

    corr_size = [6.00, 6.40]
    if 'b5' in fields:
        plot_correctors(path,
                        ids=ids, labels=labels,
                        corrector_pattern='kcdx3\.[lr]5',
                        order="5", output_id='_b5',
                        size=corr_size,
                        )
    if 'b6' in fields:
        plot_correctors(path,
                        ids=ids, labels=labels,
                        corrector_pattern='kctx3\.[lr]5',
                        order="6", output_id='_b6',
                        size=corr_size,
                        )


def plot_xing_top_ip15(fields):
    ip_str = "ip15"
    path = Path('nominal_xing_b1b4')
    ids = [f"w_ampdet_{s}_{ip_str}_{fields}" for s in ('X10', 'X10X01_cross0', 'X10X01_cross20', 'X10X01_cross50', 'X10X01Y01')]
    labels=[f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 0 $",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 20 $",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 50 $",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 0 $\n ${ltx_dqd2j('y', 'y')} $ = -35 | 7",
            ]
    output_id = f"_{ip_str}_{fields}_correction"
    # plot_detuning(
    #     path,
    #     ids=ids,
    #     labels=labels,
    #     size=[10.24, 4.80],
    #     measurement={1: {"X10": [33.2, 1.12], "Y01": [-35, 1.41]}, 4: {"X10": [4.5, 1.12], "Y01": [7, 3.2]}},
    #     beams=(1, 4),
    #     ylims={1: [-62, 62], 2: [-3, 3]},
    #     tickrotation=0,
    #     output_id=f"xing_{ip_str}_{fields}_correction"
    # )
    plot_detuning_ips(
        path,
        ids=ids,
        fields=fields,
        labels=labels,
        size=[11.24, 4.80],
        measurement={1: {"X10": [33.2, 1.12], "Y01": [-35, 1.41]}, 4: {"X10": [4.5, 1.12], "Y01": [7, 3.2]}},
        beams=(1, 4),
        ylims={1: [-62, 62], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id
    )
    corr_size = [6.00, 6.40]
    if 'b5' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr][15]', order="5", output_id=f'{output_id}_b5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr]5', order="5", output_id=f'{output_id}_b5_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr]1', order="5", output_id=f'{output_id}_b5_ip1')

    if 'b6' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=output_id)
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}_b6_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}_b6_ip1')


def plot_xing_top_ip5_ip1(fields):
    ip_str = "ip5_ip1"
    path = Path('nominal_xing_b1b4')
    ids = [f"w_ampdet_{s}_{ip_str}_{fields}" for s in ('X10', 'X10X01_cross0', 'X10X01_cross1010', 'X10X01_cross2525', 'X10X01_cross0050', 'X10X01_cross5000',  'X10X01Y01')]
    labels=[f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 0+0 $",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 10+10 $",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 25+25 $",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 50+0 $",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 0+50 $",
            f"${ltx_dqd2j('x', 'x')}$ = 33.2 | 4.5\n ${ltx_dqd2j('x', 'y')} = 0 $\n ${ltx_dqd2j('y', 'y')} =$ -35 | 7",
            ]
    output_id = f"_{ip_str}_{fields}_correction"
    # plot_detuning(
    #     path,
    #     ids=ids,
    #     labels=labels,
    #     size=[12.24, 4.80],
    #     measurement={1: {"X10": [33.2, 1.12], "Y01": [-35, 1.41]}, 4: {"X10": [4.5, 1.12], "Y01": [7, 3.2]}},
    #     beams=(1, 4),
    #     ylims={1: [-62, 62], 2: [-3, 3]},
    #     tickrotation=0,
    #     output_id=f"xing_{ip_str}_{fields}_correction"
    # )
    plot_detuning_ips(
        path,
        ids=ids,
        labels=labels,
        fields=fields,
        size=[14.24, 4.80],
        measurement={1: {"X10": [33.2, 1.12], "Y01": [-35, 1.41]}, 4: {"X10": [4.5, 1.12], "Y01": [7, 3.2]}},
        beams=(1, 4),
        ylims={1: [-62, 62], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id
    )
    corr_size = [6.00, 6.90]
    if 'b5' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr][15]', order="5", output_id=f'{output_id}_b5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr]5', order="5", output_id=f'{output_id}_b5_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kcdx3\.[lr]1', order="5", output_id=f'{output_id}_b5_ip1')

    if 'b6' in fields:
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=output_id)
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}_b6_ip5')
        plot_correctors(path, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}_b6_ip1')


if __name__ == '__main__':
    pass
