"""
Helper to extract the beta-functions at certain elements from both beams
from the model and show them.
"""
from pathlib import Path
import tfs


def main(folder, elements):
    fpath = Path(folder)
    print(folder)
    for beam in (1, 4):
        beam_path = fpath / f"b{beam}"
        df = tfs.read(beam_path / f"twiss.lhc.b{beam}.optics_ir.tfs", index="NAME")
        print(f"Beam {beam} ---")
        for element in elements:
            print(f"{element}: BETX {df.loc[element, 'BETX']}, BETY {df.loc[element, 'BETY']}")


if __name__ == '__main__':
    elements = ["MCTX.3L1", "MCTX.3R1", "MCTX.3L5", "MCTX.3R5"]
    main("commish2022_b1b4", elements)
    main("md6863", elements)
