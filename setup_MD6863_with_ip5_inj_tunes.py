"""
MD6863 - Detuning as measured during MD6863 (24.06.2022)
--------------------------------------------------------

This data is used to calculated b6 corrections.
"""
from pathlib import Path
from typing import Dict, Any, Sequence

from correction_via_amplitude_detuning_multi_xing import (
    create_optics, calculate_corrections, check_corrections
)
from utilities.utils import log_setup
from utilities.classes import (
    scaled_detuning, scaled_contraints, scaled_detuningmeasurement,
    TargetData, Target,
    DetuningMeasurement, Detuning,
)
from utilities.plotting import plot_detuning_ips, plot_correctors


def get_sum(meas_a, meas_b):
    return {beam: meas_a[beam] + meas_b[beam] for beam in meas_a.keys()}


def get_diff(meas_a, meas_b):
    return {beam: meas_a[beam] - meas_b[beam] for beam in meas_a.keys()}

# Define Machine Data
# -------------------

# Output Path
output = Path("md6863_with_ip5_inj_tunes")

# Fill in special values for crossing if needed:
xings = {
    'full': {'scheme': 'flat', 'on_x1': -160, 'on_x5': 160},
    'ip5+': {'scheme': 'flat', 'on_x5': 160},
    'ip5-': {'scheme': 'flat', 'on_x5': -160},
}

# Fill in measurement data in 10^3 m^-1:
meas_flat = {
    1: scaled_detuningmeasurement(X10=(-18, 2), X01=(23.5, 0), Y01=(0.2, 1)),
    2: scaled_detuningmeasurement(X10=(-22, 2), X01=(13.5, 0), Y01=(3.6, 0.9)),
}
meas_full = {
    1: scaled_detuningmeasurement(X10=(10, 2), X01=(31.5, 0), Y01=(1.4, 0.9)),
    2: scaled_detuningmeasurement(X10=(22, 1), X01=(-45, 0), Y01=(20, 2)),
}
meas_ip5p = {
    1: scaled_detuningmeasurement(X10=(23, 2), X01=(-1, 0), Y01=(3, 1)),
    2: scaled_detuningmeasurement(X10=(11, 1), X01=(-12.5, 0), Y01=(5, 1)),
}
meas_ip5m = {
    1: scaled_detuningmeasurement(X10=(9, 1), X01=(1.05, 0), Y01=(0.3, 0.5)),
    2: scaled_detuningmeasurement(X10=(20, 2), X01=(-19.5, 0), Y01=(-2, 2)),
}

# If one or the other was not measured (for local corrections in IP)

# meas_ip1 = get_sum(get_diff(meas_full, meas_ip5), meas_flat)
# meas_ip5 = get_sum(get_diff(meas_full, meas_ip1), meas_flat)


# Steps of calculations --------------------------------------------------------

def ltx_dqd2j(tune, action, power=1):
    if power == 1:
        return f"$Q_{{{tune},{action}}}$"
    return f"$Q_{{{tune},{action}^{{{power}}}}}$"


def get_detuning(meas: Dict[Any, DetuningMeasurement]) -> Dict[Any, Detuning]:
    return {beam: meas[beam].get_detuning() for beam in meas.keys()}


def get_targets() -> Sequence[Target]:
    # detuning_flat = get_detuning(meas_flat)
    # detuning_full = get_detuning(meas_full)
    # detuning_ip5p = get_detuning(meas_ip5p)
    # detuning_ip5m = get_detuning(meas_ip5m)
    detuning_flat = meas_flat
    detuning_full = meas_full
    detuning_ip5p = meas_ip5p
    detuning_ip5m = meas_ip5m
    targets = [
        Target(
            name="X10X01Y01_full_and_ip5pm",
            data=[
                TargetData(
                    ips=(1, 5),
                    detuning=get_diff(detuning_flat, detuning_full),
                    xing='full',
                ),
                TargetData(
                    ips=(5,),
                    detuning=get_diff(detuning_flat, detuning_ip5p),
                    xing='ip5+',
                ),
                TargetData(
                    ips=(5,),
                    detuning=get_diff(detuning_flat, detuning_ip5m),
                    xing='ip5-',
                ),
            ]
        ),
        # # TARGET TEMPLATE
        # Target(
        #     name="X10X01Y01_",
        #     data=[
        #         TargetData(
        #             ips=(1, ),
        #             detuning={
        #                 1: scaled_detuning(X10=0, X01=0, Y01=0),
        #                 2: scaled_detuning(X10=0, X01=0, Y01=0),
        #             },
        #             constraints={
        #                 1: scaled_constraints(X01="<=0"),
        #                 2: scaled_constraints(X01="<=0"),
        #             }
        #         ),
        #     ]
        # )
    ]
    # print(targets)
    # exit()
    return targets


def simulation():
    paths = {i: output / f"b{i}" for i in (1, 4)}
    lhc_beams = create_optics(paths, xings=xings, year=2022, tune_x=62.28, tune_y=60.31)
    for beams in lhc_beams.values():
        for lhcbeam in beams.values():
            lhcbeam.madx.exit()

    calculate_corrections(paths, targets=get_targets(), main_xing='full')
    check_corrections(paths, xing=xings['full'], tune_x=62.31, tune_y=60.32, year=2022, id_suffix="_col_tunes")
    check_corrections(paths, xing=xings['full'], tune_x=62.28, tune_y=60.31, year=2022, id_suffix="_inj_tunes")


def do_correction_only():
    paths = {i: output / f"b{i}" for i in (1, 4)}
    calculate_corrections(paths, targets=get_targets(), main_xing='full')
    # check_corrections(paths, xing=xings['full'], tune_x=62.31, tune_y=60.32, year=2022, id_suffix="_col_tunes")
    # check_corrections(paths, xing=xings['full'], tune_x=62.28, tune_y=60.31, year=2022, id_suffix="_inj_tunes")


def plotting():
    action_map = {"10": 'x', "01": 'y'}

    nchar = 10
    measurement = get_diff(meas_flat, meas_full)
    measurement[4] = measurement.pop(2)

    targets = get_targets()
    ids = [f"{target.name}_b6" for target in targets]
    terms = ("X10", "X01", "Y01")
    scaled = [{term: targets[0].data[0][b][term]*1e-3 for term in terms} for b in (1, 2)]
    labels = [
        "\n".join([
            f"{ltx_dqd2j(term[0], action_map[term[1:]])} = {f'{scaled[0][term]:.1f} | {scaled[1][term]}'.center(nchar)}"
            for term in terms
            ]
        )
    ]

    output_id = f"_corrections"
    plot_detuning_ips(
        output,
        ids=ids,
        labels=labels,
        fields="b6",
        size=[6., 3.9],
        measurement=measurement,
        beams=(1, 4),
        ylims={1: [-62, 62], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id,
        alternative="separate",  # "separate", "normal"
        delta=True,
    )
    corr_size = [4.80, 4.00]

    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=f'{output_id}_b6')
    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}_b6_ip5')
    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}_b6_ip1')


if __name__ == '__main__':
    log_setup()
    # simulation()
    do_correction_only()
    # plotting()
