"""
MD6863 - Detuning as measured during MD6863 (24.06.2022)
--------------------------------------------------------

This data is used to calculated b6 corrections.
"""
from pathlib import Path
from typing import Dict, Any, Sequence

from correction_via_amplitude_detuning import main as run_correction, log_setup
from classes import scaled_detuning, scaled_contraints, scaled_detuningmeasurement, TargetData, Target, \
    DetuningMeasurement, Detuning
from plotting import plot_detuning_ips, plot_correctors


def get_sum(meas_a, meas_b):
    return {beam: meas_a[beam] + meas_b[beam] for beam in meas_a.keys()}


def get_diff(meas_a, meas_b):
    return {beam: meas_a[beam] - meas_b[beam] for beam in meas_a.keys()}

# Define Machine Data
# -------------------

# Output Path
output = Path("md6863_crossterms")

# Fill in special values for crossing if needed:
xing = {'scheme': 'flat', 'on_x1': -160, 'on_x5': 160}

# Fill in measurement data in 10^3 m^-1:
meas_flat = {
    1: scaled_detuningmeasurement(X10=(-18, 2), X01=(15, 3), Y10=(32, 3), Y01=(0.2, 1)),  # X01: 15,3  32, 3
    2: scaled_detuningmeasurement(X10=(-22, 2), X01=(13, 3), Y10=(14, 3), Y01=(3.6, 0.9)),  # X01: 13, 3  14, 3
}
meas_full = {
    1: scaled_detuningmeasurement(X10=(10, 2), X01=(37, 2), Y10=(26, 2), Y01=(1.4, 0.9)),  # X01: 37, 2  26, 2
    2: scaled_detuningmeasurement(X10=(22, 1), X01=(-47, 3), Y10=(-43, 3), Y01=(20, 2)),  # X01: -47, 3  -43, 3
}
# meas_ip5 = {
#     1: scaled_detuningmeasurement(X10=(0, 0), X01=(0, 0), Y01=(0, 0)),
#     2: scaled_detuningmeasurement(X10=(0, 0), X01=(0, 0), Y01=(0, 0)),
# }
# meas_ip1 = {
#     1: scaled_detuningmeasurement(X10=(0, 0), X01=(0, 0), Y01=(0, 0)),
#     2: scaled_detuningmeasurement(X10=(0, 0), X01=(0, 0), Y01=(0, 0)),
# }

# If one or the other was not measured (for local corrections in IP)

# meas_ip1 = get_sum(get_diff(meas_full, meas_ip5), meas_flat)
# meas_ip5 = get_sum(get_diff(meas_full, meas_ip1), meas_flat)


# Steps of calculations --------------------------------------------------------

def ltx_dqd2j(tune, action, power=1):
    if power == 1:
        return f"$Q_{{{tune},{action}}}$"
    return f"$Q_{{{tune},{action}^{{{power}}}}}$"


def get_detuning(meas: Dict[Any, DetuningMeasurement]) -> Dict[Any, Detuning]:
    return {beam: meas[beam].get_detuning() for beam in meas.keys()}


def get_targets() -> Sequence[Target]:
    detuning_flat = get_detuning(meas_flat)
    detuning_full = get_detuning(meas_full)
    targets = [
        Target(
            name="X10X01Y01_global",
            data=[
                TargetData(
                    ips=(1, 5),
                    detuning=get_diff(detuning_flat, detuning_full),
                ),
            ]
        ),
        # # TARGET TEMPLATE
        # Target(
        #     name="X10X01Y01_",
        #     data=[
        #         TargetData(
        #             ips=(1, ),
        #             detuning={
        #                 1: scaled_detuning(X10=0, X01=0, Y01=0),
        #                 2: scaled_detuning(X10=0, X01=0, Y01=0),
        #             },
        #             constraints={
        #                 1: scaled_constraints(X01="<=0"),
        #                 2: scaled_constraints(X01="<=0"),
        #             }
        #         ),
        #     ]
        # )
    ]
    # print(targets)
    # exit()
    return targets


def simulation():
    paths = {i: output / f"b{i}" for i in (1, 4)}
    run_correction(paths, xing=xing, targets=get_targets(), year=2022)


def plotting():
    action_map = {"10": 'x', "01": 'y'}

    nchar = 10
    measurement = get_diff(meas_flat, meas_full)
    measurement[4] = measurement.pop(2)

    targets = get_targets()
    ids = [f"{target.name}_b6" for target in targets]
    terms = ("X10", "X01", "Y01")
    scaled = [{term: targets[0].data[0][b][term]*1e-3 for term in terms} for b in (1, 2)]
    labels = [
        "\n".join([
            f"{ltx_dqd2j(term[0], action_map[term[1:]])} = {f'{scaled[0][term]:.1f} | {scaled[1][term]}'.center(nchar)}"
            for term in terms
            ]
        )
    ]

    output_id = f"_corrections"
    plot_detuning_ips(
        output,
        ids=ids,
        labels=labels,
        fields="b6",
        size=[6., 3.9],
        measurement=measurement,
        beams=(1, 4),
        ylims={1: [-62, 62], 2: [-3, 3]},
        tickrotation=0,
        output_id=output_id,
        alternative="separate",  # "separate", "normal"
        delta=True,
    )
    corr_size = [4.80, 4.00]

    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr][15]', order="6", output_id=f'{output_id}_b6')
    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]5', order="6", output_id=f'{output_id}_b6_ip5')
    plot_correctors(output, ids=ids, labels=labels, size=corr_size, corrector_pattern='kctx3\.[lr]1', order="6", output_id=f'{output_id}_b6_ip1')


if __name__ == '__main__':
    log_setup()
    simulation()
    plotting()
