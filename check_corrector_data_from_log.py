"""
Helper to get the corrector data from the logs and print them as a latex-table.
Getting them from the log to immediately also get the % in strength.
"""

from pathlib import Path
import re

import pandas as pd
import tfs


def main(logfile: Path):
    log = Path(logfile).read_text()
    columns = [f"{n}{v}" for n in ("KCTX3.L1", "KCTX3.R1", "KCTX3.L5", "KCTX3.R5") for v in ("VALUE", "PERCENT")]
    df = pd.DataFrame(columns=columns)
    lines = log.split("\n")
    for idx, line in enumerate(lines):
        if line.strip().startswith("DEBUG | Attempting to write file:"):
            if lines[idx+1].strip().startswith("INFO | Current Detuning Values"):
                series = pd.Series(index=columns, dtype=object)
                name = re.search(r"ampdet\.lhc\.b\d\.([^.]+)\.", line).group(1)
                # if name == "nominal" or not name.endswith("_b6"):
                if name == "nominal":
                        continue

                for elem_idx in range(4):
                    elem_line = re.split(r"\s+", lines[idx+6+elem_idx])
                    corr, val, perc = elem_line[3], elem_line[4], elem_line[6]
                    series[f"{corr}VALUE"] = val
                    series[f"{corr}PERCENT"] = perc
                df.loc[name, :] = series

    print_mytable(df)
    # tfs.write_tfs("gathered_corrector_values.tfs", df, save_index="SCENARIO")


def print_mytable(df: pd.DataFrame):
    for name, row in df.iterrows():
        latex_name = name.replace("_", r"\_")
        # values = [fr"\num{{{row[f'{c}VALUE']}}} ({row[f'{c}PERCENT'][:-1]}\%)" for c in ("KCTX3.L1", "KCTX3.R1", "KCTX3.L5", "KCTX3.R5")]
        values = [fr"{float(row[f'{c}VALUE'])*1e-3: .3f} & ({abs(int(row[f'{c}PERCENT'][:-1]))}\%)" for c in ("KCTX3.L1", "KCTX3.R1", "KCTX3.L5", "KCTX3.R5")]
        print(fr"{latex_name} & {' & '.join(values)}\\")


if __name__ == '__main__':
    # main(logfile="/home/jdilly/Software/b6_correction_from_amplitude_detuning/xing_b1b4/b1/full_output.log")
    # main(logfile="/home/jdilly/Software/b6_correction_from_amplitude_detuning/xing_measured_b1b4/b4/full_output.log")
    # main(logfile="/home/jdilly/Software/b6_correction_from_amplitude_detuning/commish2022_b1b4/b4/full_output.log")
    # main(logfile="/home/jdilly/Software/b6_correction_from_amplitude_detuning/md6863/b4/full_output.log")
    # main(logfile="/home/jdilly/Software/b6_correction_from_amplitude_detuning/md6863_with_ip5/b4/full_output.log")
    main(logfile="/home/jdilly/Software/b6_correction_from_amplitude_detuning/md6863_with_ip5_inj_tunes/b4/full_output.log")
